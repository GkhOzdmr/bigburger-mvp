package com.bigburger.presentation.product

import com.bigburger.common.NetworkUtil
import com.bigburger.data.model.BasketProductModel
import com.bigburger.data.model.ProductModel
import com.bigburger.data.remote.NetworkErrorType
import com.bigburger.data.repository.ProductRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import timber.log.Timber
import java.net.ConnectException
import java.net.PortUnreachableException
import java.net.UnknownHostException
import javax.inject.Inject

/**
 * [ProductContract.Presenter] implementation class for [ProductFragment]
 *
 *
 * @property basketProductList List of [BasketProductModel]
 * @property view [ProductContract.View] implementation instance see [ProductFragment]
 * @property disposable RxJava Subscriptions container
 */
class ProductPresenter @Inject constructor(
    private val productRepository: ProductRepository
) :
    ProductContract.Presenter {

    private var basketProductList = mutableListOf<BasketProductModel>()
    private var view: ProductContract.View? = null

    private var disposable = CompositeDisposable()

    override fun onCreate() {
        getProducts()
    }

    override fun onDestroy() {
        dropView()
        disposeSubscriptions()
    }

    override fun takeView(view: ProductContract.View) {
        this.view = view
    }

    override fun dropView() {
        this.view = null
    }

    override fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

    /**
     * Retrieves the products from remote service & and updates the view
     */
    override fun getProducts() {
        disposable.add(
            productRepository.getProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view?.showLoading() }
                .doAfterTerminate { view?.hideLoading() }
                .subscribe({ response: Response<List<ProductModel>>? ->
                    Timber.d("response: ${response.toString()}")
                    when {
                        response!!.body() != null -> view?.populateRecyclerView(response.body() as List<ProductModel>)
                        response.errorBody() != null -> {
                            view?.populateRecyclerView(emptyList())
                            view!!.displayErrorMessage(NetworkErrorType.UnknownError)
                        }
                        else -> view?.populateRecyclerView(emptyList())
                    }
                }, {
                    view?.populateRecyclerView(emptyList())
                    view!!.displayErrorMessage(NetworkErrorType.UnknownError)
                })
        )
    }

    /**
     * Adds product to [basketProductList] & updates View for Basket unique product amount
     *
     * If added item's product type and ingredients are matching with the ones that already
     * exists in the list, then only the product amount will change to sum of new and previous value.
     */
    override fun addToBasket(basketProductModel: BasketProductModel) {
        // This boolean will be true if item matches(ingredients and product type both should match)
        var itemMatches = false
        // To track current index
        var currentIndex = 0
        // This will be used if the item is matched
        var matchingIndex = 0

        disposable.addAll(
            Observable.fromIterable(basketProductList)
                .takeWhile {
                    // Terminates the subscription and calls onComplete
                    // if the condition is no longer matches

                    // Note: doOnComplete or doOnTerminate doesn't get called whenever
                    // takeWhile terminates subscription.
                    // Hence, have to implement onNext and onComplete at the
                    // .subscribe call site.
                    !itemMatches
                }
                .subscribe(
                    { //onNext
                        Timber.d("onNext: currentIndex: $currentIndex")
                        // If item type and ingredients matches, boolean will be true so that we can
                        // update the product amount at the end of the stream.
                        if (basketProductModel.ingredients.hashCode() == it.ingredients.hashCode()
                            && basketProductModel.productModel.hashCode() == it.productModel.hashCode()
                        ) {
                            itemMatches = true
                            matchingIndex = currentIndex


                        }
                        currentIndex++
                    },
                    {},
                    { //onComplete
                        Timber.d("doOnComplete")
                        // This is invoked when the item already exists. Change the amount value to
                        // sum of previous and new amount.
                        if (itemMatches) {
                            Timber.d("Pre amount: ${basketProductList[matchingIndex].amount}")
                            basketProductList[matchingIndex].amount += basketProductModel.amount
                            Timber.d("Post amount: ${basketProductList[matchingIndex].amount}")
                        } else {
                            // If item does not match with current items add as new item.
                            basketProductList.add(basketProductModel)
                        }

                        // Update the view
                        view!!.updateBasketLogo(basketProductList.size)
                    })
        )
    }

    /**
     * Replaces the list of [BasketProductModel] at presenter with given list
     *
     * @param basketProducts list of [BasketProductModel]
     */
    override fun setBasketProducts(basketProducts: List<BasketProductModel>) {
        this.basketProductList = basketProducts.toMutableList()
        Timber.d("basketProductList size: ${basketProductList.size}")
        view!!.updateBasketLogo(basketProductList.size)
    }

    /**
     * Returns the products that is added to the list with amount and ingredient data
     *
     * @return list of [BasketProductModel]
     */
    override fun getBasketProducts(): List<BasketProductModel> {
        return basketProductList
    }
}