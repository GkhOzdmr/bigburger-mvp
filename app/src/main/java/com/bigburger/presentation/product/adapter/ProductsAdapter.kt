package com.bigburger.presentation.product.adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bigburger.R
import com.bigburger.common.GlideUtil
import com.bigburger.common.ImageHelper
import com.bigburger.common.PriceUtil
import com.bigburger.data.model.ProductModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import kotlinx.android.synthetic.main.item_product.view.*

/**
 * Recycler View Adapter class for displaying [ProductModel]
 *
 * @property context Current context of which this class initiated.
 * @property items list of [ProductModel]
 */
class ProductsAdapter(
    private val context: Context,
    private val notifier: Notifier) :
    RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    /**
     * Interface for notifying implementers for Adapter actions
     */
    interface Notifier{
        /**
         * Invoked whenever specific adapter item clicked,
         *
         * @param item ProductModel item on adapter that is selected
         */
        fun productItemSelected(item: ProductModel)
    }

    private var items = listOf<ProductModel>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Sets the product name to proper TextView
        holder.tv_product_name.text = items[position].title

        // Places img from link on ImageView, if there is error places default image
//        GlideUtil.displayImage(Uri.parse(items[position].thumbnail), null, context, holder.img_product)
        GlideUtil.displayImageRoundCornered(Uri.parse(items[position].thumbnail), context, holder.img_product)

        // Sorts price as long and places proper string to TextView
        holder.tv_product_price.text = PriceUtil.priceWithSymbol(context, items[position].price)

        // Notifies if item on adapter clicked to implementors
        holder.itemView.setOnClickListener {
            notifier.productItemSelected(items[position])
        }
    }

    /**
     * Updates the items of this adapter
     *
     * @param items List of [ProductModel]
     */
    fun updateItems(items: List<ProductModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    /**
     * View handler class of this adapter
     *
     * @param view Display layout of this adapter. See [R.layout.item_product]
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_product_name = view.tv_product_name
        val img_product = view.img_product
        val view_horizontal = view.view_horizontal
        val img_chevron = view.img_chevron
        val tv_product_price = view.tv_product_price
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_product, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

}
