package com.bigburger.presentation.product.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager

import com.bigburger.R
import com.bigburger.common.*
import com.bigburger.data.model.ProductModel
import com.bigburger.presentation.MainActivity
import com.bigburger.presentation.product.ProductContract
import com.bigburger.data.model.BasketProductListEvent
import com.bigburger.data.model.BasketProductModel
import com.bigburger.data.remote.NetworkErrorType
import com.bigburger.presentation.basket.fragment.BasketFragment
import com.bigburger.presentation.error.ErrorFragment
import com.bigburger.presentation.product.adapter.ProductsAdapter
import com.bigburger.presentation.productorder.fragment.ProductOrderFragment
import com.jakewharton.rxbinding3.view.clicks
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_nav_layout.view.*
import kotlinx.android.synthetic.main.fragment_product.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import timber.log.Timber
import javax.inject.Inject

/**
 * [Fragment] implementation.
 * Use the [ProductFragment.newInstance] factory method to
 * create an instance of this fragment.

 * User can see the list of [ProductModel] that's fetched from API
 *
 * @property presenter presenter implementation for handling logic
 * @property disposable RxJava Subscriptions container
 */
class ProductFragment :
    DaggerFragment(),
    ProductContract.View,
    ProductsAdapter.Notifier {

    @Inject
    lateinit var presenter: ProductContract.Presenter
    private val disposable = CompositeDisposable()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        presenter.takeView(this)
        requestProducts()
    }

    override fun onResume() {
        super.onResume()
        presenter.getBasketProducts().forEach {
            Timber.d(
                "Product title: %s ingredients: %s amount: %s",
                it.productModel!!.title, it.ingredients, it.amount
            )
        }
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDestroy()
        disposeSubscriptions()
    }

    /**
     * Disposes any subscription that's not disposed. Should be called at onDestroy/onStop.
     */
    private fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

    /**
     * Checks the Network Connection availability and calls presenter for starting api call
     */
    private fun requestProducts() {
        // If connection available, calls presenter for making API request
        presenter.getProducts()
//        disposable.add(
//            NetworkUtil.hasInternetConnection()
//                .doOnSubscribe {
//                    showLoading()
//                }
//                .doAfterTerminate {
//                    hideLoading()
//                    btn_try_again.isEnabled = true
//                }
//                .subscribe { networkAvailable ->
//                    if (networkAvailable) {
//
//                    } else {
//                        // Displays a dialog notifying user about no
//                        // network connection established hence request wasn't made
//                        displayErrorMessage(NetworkErrorType.NoConnection)
//                    }
//                }
//        )
    }

    /**
     * Sets various listeners for views and other framework tools
     */
    private fun setListeners() {
        (activity as MainActivity).apply {
            bottom_nav.btn_basket.setOnClickListener {
                openBasketFragment()
            }
        }

        disposable.add(
            btn_try_again.clicks()
                .subscribe {
                    btn_try_again.isEnabled = false
                    requestProducts()
                }
        )
    }

    /**
     * EventBus listener for [BasketProductModel]
     *
     * EventBust.post() call sites: [ProductOrderFragment]
     *
     * @param basketProductModel Event type
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun observeBasketProductModel(basketProductModel: BasketProductModel) {
        presenter.addToBasket(basketProductModel)
    }

    /**
     * EventBus listener for list of [BasketProductModel]
     *
     * EventBust.post() call sites: [BasketPresenter]
     *
     * @param basketProductListEvent Event type
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun observeBasketProductList(basketProductListEvent: BasketProductListEvent) {
        // This is called when user removes item from their basket,
        // update happens because the list of products on ProductPresenter will be the old one
        // and if user returns back from BasketFragment and re-navigates to it, this presenters list will be
        // shown and remove will have no effect.
        // Hence we change the list of ProductPresenter.
        presenter.setBasketProducts(basketProductListEvent.basketProducts)
    }

    /**
     * EventBus listener for [BasicEvent]
     *
     * EventBust.post() call sites: [ErrorFragment]
     *
     * @param basicEvent Event type
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun observeBasicEvent(basicEvent: BasicEvent) {
        Timber.d("observeBasicEvent called")
        // When user selected Try Again on ErrorFragment
        if (basicEvent == BasicEvent.TryAgain) {
            requestProducts()
        }
    }

    /**
     * Navigates to [BasketFragment] with the selected products
     */
    private fun openBasketFragment() {
        (activity as MainActivity).changeFragment(
            BasketFragment.newInstance(presenter.getBasketProducts()),
            fullScreen = true
        )
    }

    /**
     * Populates the RecyclerView [rc_products] with the given list of [ProductModel]
     * If the list is empty, displays "empty message" and try again button
     *
     * @param products list of [ProductModel]
     */
    override fun populateRecyclerView(products: List<ProductModel>) {
        if (products.isNotEmpty()) {
            // Making sure "no items found" state views are not displayed, and recyclerview is visible
            group_no_data.visibility = View.GONE
            rc_products.visibility = View.VISIBLE

            //Initiating adapter class of rc_products and assigning @products to it
            rc_products.adapter = ProductsAdapter(context!!, this).apply {
                updateItems(products)
            }

            //Sets the layout type of rc_products to GridLayout with max 2 items at each line
            rc_products.layoutManager = GridLayoutManager(context!!, 2)

            rc_products.addItemDecoration(ItemDecoration(8, 8, 8, 8))

        } else {
            displayNoItemsView()
        }

    }

    /**
     * Displays views for "no items found" state
     */
    private fun displayNoItemsView() {
        group_no_data.visibility = View.VISIBLE
        rc_products.visibility = View.GONE
    }

    /**
     * This function is over ridden from ProductAdapter.Notifier interface
     *
     * When clicking on adapter item, this function is called with the respective [ProductModel] object
     * that is belong to that item.
     *
     * At this implementation, ProductOrderFragment will be displayed with the given item.
     * User will be able to review the details of this item,
     * choose extra items on top of the respected [ProductModel] such as
     * ketchup, mayonnaise etc. and add the product to the basket with additional items.
     *
     * @param item ProductModel object wherever the adapter item click happened
     */
    override fun productItemSelected(item: ProductModel) {
        (activity as MainActivity).changeFragment(ProductOrderFragment.newInstance(item), true)
    }

    override fun showLoading() {
        (activity as MainActivity).showLoading()
    }

    override fun hideLoading() {
        (activity as MainActivity).hideLoading()
    }

    /**
     * Updates the unique product amount on basket fab at BottomNavBar
     */
    override fun updateBasketLogo(totalProductType: Int) {
        Timber.d("totalProductType: $totalProductType")
        if (totalProductType != 0) {
            //Make text view visible and set the amount of unique product type
            val tv_product_unique_amount =
                (activity as MainActivity).bottom_nav.tv_product_unique_amount
            tv_product_unique_amount.apply {
                visibility = View.VISIBLE
                text = totalProductType.toString()
            }
        } else {
            // If there are no products, hide the amount text view
            (activity as MainActivity).bottom_nav.tv_product_unique_amount.apply {
                visibility = View.GONE
            }
        }
    }

    /**
     * Opens a new dialog screen that shows error message,
     * user can dismiss the dialog or can try making request again.
     *
     * @param errorType error type for receiving message from string res
     */
    override fun displayErrorMessage(errorType: ErrorType) {
        displayNoItemsView()
        // Gets message string with given error type
        val errorModel = ErrorMessage.getMessage(context!!, errorType)
        // Opens ErrorFragment with ErrorModel
        val fm = (activity as MainActivity).supportFragmentManager
        ErrorFragment.newInstance(errorModel).show(fm, ErrorFragment::class.java.simpleName)
    }

    companion object {
        /**
         * Creates new instance of ProductFragment
         */
        @JvmStatic
        fun newInstance() =
            ProductFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
