package com.bigburger.presentation.product

import com.bigburger.base.BasePresenter
import com.bigburger.base.BaseView
import com.bigburger.common.ErrorType
import com.bigburger.data.model.BasketProductModel
import com.bigburger.data.model.ProductModel
import io.reactivex.Single

interface ProductContract {

    interface View : BaseView<Presenter> {

        /**
         * Show loading layout
         */
        fun showLoading()

        /**
         * Hide loading layout
         */
        fun hideLoading()

        /**
         * Populate Recycler View with [ProductModel]
         *
         * @param products list of [ProductModel] to display at RecyclerView
         */
        fun populateRecyclerView(products: List<ProductModel>)

        /**
         * Updates the product count of Basket Button on [ProductFragment]
         *
         * @param amount of unique product types that's present in the basket
         */
        fun updateBasketLogo(totalProductType: Int)

        fun displayErrorMessage(errorType: ErrorType)
    }

    interface Presenter : BasePresenter<View> {

        /**
         * Get list of [ProductModel] from Service
         */
        fun getProducts()

        /**
         * Adds the product to basket list
         *
         * @param basketProductModel [BasketProductModel] to add to list of [BasketProductModel]
         */
        fun addToBasket(basketProductModel: BasketProductModel)

        /**
         * Retrieves the list of [BasketProductModel] to display at basket
         *
         * @return list of [BasketProductModel] that is retrieved from product selections
         */
        fun getBasketProducts(): List<BasketProductModel>

        /**
         * Replaces the list of [BasketProductModel] at presenter with given list
         *
         * @param basketProducts list of [BasketProductModel]
         */
        fun setBasketProducts(basketProducts: List<BasketProductModel>)
    }
}