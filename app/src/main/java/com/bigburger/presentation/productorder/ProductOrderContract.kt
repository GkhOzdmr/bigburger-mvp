package com.bigburger.presentation.productorder

import com.bigburger.base.BasePresenter
import com.bigburger.base.BaseView
import com.bigburger.data.model.BasketProductModel
import com.bigburger.data.model.IngredientModel
import com.bigburger.data.model.ProductModel
import io.reactivex.Observable
import io.reactivex.Single

interface ProductOrderContract {

    interface View : BaseView<Presenter> {
        fun displayProduct(product: ProductModel)
        fun displayIngredients(ingredients: List<IngredientModel>)
        fun updatePriceText(priceAsLong: Long)
        fun updateProductAmount(productAmount: Int)
    }

    interface Presenter : BasePresenter<View> {
        fun setProductModel(product: ProductModel)
        fun getIngredientsForProduct(productRef: Int)
        fun getSelectedIngredients(): Observable<List<IngredientModel>>
        fun decreaseProductAmount()
        fun increaseProductAmount()
        fun getProductAmount(): Int
        fun getProductModel(): ProductModel
        fun getIngredientString(ingredientModelList: List<IngredientModel>): Single<String>
        fun controlAndSetAmount(productAmount: Int)
    }

}