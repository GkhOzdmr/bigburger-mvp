package com.bigburger.presentation.productorder.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bigburger.R
import com.bigburger.data.model.IngredientModel
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.android.synthetic.main.item_ingredient.view.*

/**
 * Recycler View Adapter class for displaying [IngredientModel]
 *
 * @property context Current context of which this class initiated.
 * @property items list of [IngredientModel]
 */
class IngredientAdapter(
    private val context: Context
) : RecyclerView.Adapter<IngredientAdapter.ViewHolder>() {

    private var items = listOf<IngredientModel>()
    private val itemSelectorMap = mutableMapOf<Int, Boolean>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setSelection(position, holder)
        holder.tv_ingredient_name.apply {
            // Sets the ingredient name
            text = items[position].ingredientName
            // When clicked, notifies implementor for item click with the respective element of items
            setOnClickListener {
                actuator(position)
                setSelection(position, holder)
            }
        }
    }

    /**
     * Sets the boolean value of the given position to opposite of its current value
     *
     *@param position position of item click
     */
    private fun actuator(position: Int) {
        itemSelectorMap[position] = !itemSelectorMap[position]!!
    }

    /**
     * Sets the given items state according to actuator value
     *
     * @param position position of item click
     * @param holder ViewHolder of this adapter
     */
    private fun setSelection(position: Int, holder: ViewHolder) {
        if (itemSelectorMap[position]!!) {
            holder.tv_ingredient_name.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        } else {
            holder.tv_ingredient_name.paintFlags = Paint.LINEAR_TEXT_FLAG
        }
    }

    /**
     * Updates the items of this adapter
     *
     * @param items List of [IngredientModel]
     */
    fun updateItems(items: List<IngredientModel>) {
        this.items = items
        // Sets the map with given items for select actuator
        items.forEachIndexed { index, _ -> itemSelectorMap[index] = false }
        notifyDataSetChanged()
    }

    /**
     * Iterates the [itemSelectorMap] if value is false that means item is wanted.
     * False value items added to the new list and returned as observable.
     *
     * @return Observable list of [IngredientModel]
     */
    fun getSelectedIngredients(): List<IngredientModel> {
        val selecteds = mutableListOf<IngredientModel>()
        for ((key, value) in itemSelectorMap) {
            if (!value) selecteds.add(items[key])
        }
        return selecteds
    }

    /**
     * View handler class of this adapter
     *
     * @param view Display layout of this adapter. See [R.layout.item_ingredient]
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_ingredient_name: TextView = view.tv_ingredient_name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_ingredient, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

}
