package com.bigburger.presentation.productorder.fragment

import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.bigburger.R
import com.bigburger.common.GlideUtil
import com.bigburger.common.PriceUtil
import com.bigburger.data.model.IngredientModel
import com.bigburger.data.model.ProductModel
import com.bigburger.common.ItemDecoration
import com.bigburger.data.model.BasketProductModel
import com.bigburger.presentation.MainActivity
import com.bigburger.presentation.productorder.ProductOrderContract
import com.bigburger.presentation.productorder.adapter.IngredientAdapter
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_product_order.*
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import javax.inject.Inject

/**
 * Argument constants to be used in [ProductOrderFragment]
 */
private const val ARG_PRODUCT_MODEL = "productModel"

/**
 * [Fragment] implementation.
 * Use the [ProductOrderFragment.newInstance] factory method to
 * create an instance of this fragment.

 * User can review the specific [ProductModel] that's selected by him/her,
 * choose ingredients and amount of productthat he/she wants and add product to basket.
 *
 * @property presenter presenter implementation for handling logic
 * @property disposable RxJava Subscriptions container
 */
class ProductOrderFragment :
    DaggerFragment(),
    ProductOrderContract.View {

    @Inject
    lateinit var presenter: ProductOrderContract.Presenter
    private var disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        presenter.takeView(this)
        presenter.onCreate()
        arguments?.let {
            presenter.setProductModel(it.getParcelable(ARG_PRODUCT_MODEL)!!)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDestroy()
        disposeSubscriptions()
    }

    /**
     * Sets up the listeners for components
     */
    private fun setListeners() {
        btn_add_to_basket.setOnClickListener {
            btn_add_to_basket.isEnabled = false
            addToBasketAndNavigate()
        }

        img_decrease_amount.setOnClickListener {
            presenter.decreaseProductAmount()
        }

        img_increase_amount.setOnClickListener {
            presenter.increaseProductAmount()
        }

        //Text listener for updating product amount
        edt_product_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val editTextInput = edt_product_amount.text.toString()
                if (editTextInput != "") {
                    presenter.controlAndSetAmount(edt_product_amount.text.toString().toInt())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        // When losing focus, if input is empty sets it to 1
        // other unwanted conditions is also handled here
        edt_product_amount.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                controlAmountInput()
            }
        }
    }

    /**
     * Checks the Edit Text input. If field is empty, sets the amount to 1.
     * If not empty, controls the input and sets
     */
    private fun controlAmountInput() {
        val editTextInput = edt_product_amount.text.toString()
        if (editTextInput != "") {
            presenter.controlAndSetAmount(edt_product_amount.text.toString().toInt())
        } else presenter.controlAndSetAmount(1)
    }

    /**
     * Updates the product price by given [Long] value
     *
     * @param priceAsLong multiplication of product price and product amount
     */
    override fun updatePriceText(priceAsLong: Long) {
        tv_product_price.text = PriceUtil.priceWithSymbol(context!!, priceAsLong)
    }

    /**
     * Updates the product amount
     *
     * @param productAmount amount of product
     */
    override fun updateProductAmount(productAmount: Int) {
        edt_product_amount.setText(productAmount.toString())
    }

    private fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

    /**
     * Displays selected products info(thumbnail, price, name etc.)
     *
     * @param product product that is selected and delivered via fragment argument
     */
    override fun displayProduct(product: ProductModel) {
        tv_product_name.text = product.title
        tv_product_price.text = PriceUtil.priceWithSymbol(context!!, product.price)
        tv_product_description.text = product.description

        GlideUtil.displayImage(Uri.parse(product.thumbnail), null, context!!, img_product)
    }

    /**
     * Gets the ingredients by the type of product, displays it in recycler view.
     *
     * User can decide if they do/don't want the specific ingredients.
     *
     * @param ingredients ingredients that user selected
     */
    override fun displayIngredients(ingredients: List<IngredientModel>) {
        if (ingredients.isNotEmpty()) {
            cl_product_additions.visibility = View.VISIBLE

            val ingredientAdapter = IngredientAdapter(context!!)

            rc_ingredients.apply {
                adapter = ingredientAdapter
                layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                addItemDecoration(ItemDecoration(4, 4, 4, 4))
            }

            ingredientAdapter.updateItems(ingredients)

        } else {
            group_ingredients.visibility = View.GONE
        }
    }

    /**
     * Notifies [ProductFragment] with [basketProductModel] and navigates back to it
     */
    private fun navigateToProductFragment(basketProductModel: BasketProductModel) {
        EventBus.getDefault().post(basketProductModel)
        (activity as MainActivity).supportFragmentManager.popBackStack()
    }

    /**
     * Sends the item's type, amount and ingredients info to ProductFragment to later be
     * used for checkout & navigates to [ProductFragment]
     */
    private fun addToBasketAndNavigate() {
        val basketProductModel = BasketProductModel(
            productModel = presenter.getProductModel(),
            ingredients = "",
            amount = presenter.getProductAmount()
        )
        if (rc_ingredients.adapter != null) {
            val ingredientAdapter = rc_ingredients.adapter as IngredientAdapter

            disposable.addAll(
                presenter.getIngredientString(ingredientAdapter.getSelectedIngredients())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ str ->
                        // Prevent button click
                        btn_add_to_basket.isEnabled = false
                        Timber.d("str: $str")
                        // Control input, if miss-type fix it and set the amount
                        controlAmountInput()

                        basketProductModel.ingredients = str
                        navigateToProductFragment(basketProductModel)
                    }, {
                        Timber.d("it.localizedMessage ${it.localizedMessage}")
                    })
            )
        } else {
            navigateToProductFragment(basketProductModel)
        }

    }

    companion object {

        /**
         * Creates new instance of [ProductOrderFragment]
         *
         * @param productModel product that is selected at [ProductFragment]
         */
        @JvmStatic
        fun newInstance(productModel: ProductModel) =
            ProductOrderFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PRODUCT_MODEL, productModel)
                }
            }

    }
}
