package com.bigburger.presentation.productorder

import com.bigburger.data.local.IngredientType
import com.bigburger.data.model.BasketProductModel
import com.bigburger.data.model.IngredientModel
import com.bigburger.data.model.ProductModel
import com.bigburger.data.repository.ProductOrderRepository
import com.bigburger.presentation.MainActivity
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import javax.inject.Inject

/**
 * [ProductOrderContract.Presenter] implementation for [ProductOrderFragment]
 *
 * @property view [ProductOrderContract.View] implementation instance
 * @property productOrderRepository repository for ProductOrder
 * @property productModel [ProductModel] that comes from arguments of [ProductOrderFragment]
 * @property productAmount Amount of products that is requested by user
 * @property disposable RxJava Subscriptions container
 */
class ProductOrderPresenter @Inject constructor(
    private val productOrderRepository: ProductOrderRepository
) : ProductOrderContract.Presenter {

    private var view: ProductOrderContract.View? = null
    private var disposable = CompositeDisposable()

    private var productModel: ProductModel? = null
    private var productAmount: Int = 1

    override fun onCreate() {

    }

    override fun onDestroy() {
        dropView()
        disposeSubscriptions()
    }

    override fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

    override fun takeView(view: ProductOrderContract.View) {
        this.view = view
    }

    override fun dropView() {
        this.view = null
    }

    /**
     * Sets the [ProductModel] of this class
     *
     * @param product [ProductModel] to set
     */
    override fun setProductModel(product: ProductModel) {
        this.productModel = product
        view!!.displayProduct(product)
        getIngredientsForProduct(product.ref.toInt())
    }

    /**
     * Depending on product type, retrieves specific ingredients from repository
     *
     * @param productRef product ref for picking ingredient type
     */
    override fun getIngredientsForProduct(productRef: Int) {
        when (productRef) {

            1, 2, 3, 4, 5, 6 -> {
                productOrderRepository.getIngredients(IngredientType.HamburgerIngredients)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe {
                        view!!.displayIngredients(it)
                    }
            }

            9, 10 -> {
                productOrderRepository.getIngredients(IngredientType.DrinkIngredients)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe {
                        view!!.displayIngredients(it)
                    }
            }

            else -> {
                view!!.displayIngredients(emptyList())
            }

        }
    }

    /**
     * Returns selected ingredients as observable
     *
     * @return observable list of [IngredientModel]
     */
    override fun getSelectedIngredients(): Observable<List<IngredientModel>> {
        return Observable.fromArray()
    }

    /**
     * Decreases [productAmount] by one, updates view.
     *
     * If [productAmount] is 1, decrease won't happen
     */
    override fun decreaseProductAmount() {
        if (productAmount != 1) productAmount -= 1
        else productAmount = 1

        updateAmountChange()
    }

    /**
     * Increases [productAmount] by one, updates view.
     *
     * If [productAmount] is 99, increase won't happen
     */
    override fun increaseProductAmount() {
        if (productAmount != 99) productAmount += 1
        else productAmount = 99

        updateAmountChange()
    }

    /**
     * Checks if product amount input is empty or equal to zero.
     * If it's less than or equal to 0, sets it to 1
     * If it's more than 99, sets it to 99
     *
     * If none above, then product amount set to input
     */
    override fun controlAndSetAmount(productAmount: Int) {
        when {
            productAmount <= 0 -> setProductAmount(1)
            productAmount > 99 -> setProductAmount(99)
            else -> setProductAmount(productAmount)
        }
    }

    /**
     * Replaces the [productAmount] with the given value, updates the price view
     *
     * @param productAmount new product amount value
     */
    private fun setProductAmount(productAmount: Int) {
        this.productAmount = productAmount
        view?.let{
            it.updatePriceText(productModel!!.price * productAmount)
        }
    }

    /**
     * Updates the view for amount and price changes
     */
    private fun updateAmountChange() {
        view!!.updatePriceText(productModel!!.price * productAmount)
        view!!.updateProductAmount(productAmount)
    }

    /**
     * @return [ProductModel] product model that belongs to this instance of [ProductOrderFragment]
     */
    override fun getProductModel(): ProductModel {
        return this.productModel!!
    }

    /**
     * @return Product amount as integer
     */
    override fun getProductAmount(): Int {
        return this.productAmount
    }

    override fun getIngredientString(ingredientModelList: List<IngredientModel>): Single<String> {
        return if (ingredientModelList.isNotEmpty()) {
            var ingredientString = ""
            ingredientModelList.forEachIndexed { index, ingredientModel ->
                ingredientString += if (index != ingredientModelList.size - 1) {
                    "${ingredientModel.ingredientName}, "
                } else {
                    "${ingredientModel.ingredientName}"
                }
            }
            Single.just(ingredientString).subscribeOn(Schedulers.io())
        } else {
            Single.just("").subscribeOn(Schedulers.io())
        }
    }
}


