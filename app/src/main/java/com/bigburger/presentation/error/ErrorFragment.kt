package com.bigburger.presentation.error

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import com.bigburger.R
import com.bigburger.common.BasicEvent

import com.bigburger.data.model.ErrorModel
import kotlinx.android.synthetic.main.fragment_error.*
import org.greenrobot.eventbus.EventBus


/**
 * Argument constants to be used in [ErrorFragment]
 */
const val ARG_ERROR_MODEL = "errorModel"

/**
 * [AppCompatDialogFragment] implementation.
 * Use the [ErrorFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 * Used to display error messages.
 *
 * @property errorModel [ErrorModel] that contains error header and message
 */
class ErrorFragment: AppCompatDialogFragment() {

    lateinit var errorModel: ErrorModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            errorModel = it.getParcelable<ErrorModel>(ARG_ERROR_MODEL)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_error, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }

    /**
     * Initializes views, putting texts into TextViews etc.
     */
    private fun initView() {
        tv_error_header.text = errorModel.errorHeader
        tv_error_message.text = errorModel.errorMessage
    }

    /**
     * Sets various listeners for views
     */
    private fun setListener() {

        btn_close_bottom.setOnClickListener {
            // Closes the dialog
            dismiss()
        }

        btn_try_again.setOnClickListener {
            // Sends try again notification to listeners and closes the dialog
            EventBus.getDefault().post(BasicEvent.TryAgain)
            dismiss()
        }
    }


    companion object {

        /**
         * Returns an instance with given arguments
         *
         * @param errorModel [ErrorModel] to display on ErrorFragment
         *
         * @return instance of [ErrorFragment]
         */
        @JvmStatic
        fun newInstance(errorModel: ErrorModel) =
            ErrorFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_ERROR_MODEL, errorModel)
                }
            }
    }
}
