package com.bigburger.presentation

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.bigburger.R
import com.bigburger.presentation.product.fragment.ProductFragment
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.loading_layout.view.*

/**
 * Container Activity of Single Activity application
 *
 * Fragments will be replaced from this Activity
 *
 * App bars, navigation bar, navigation drawer and other common screen views
 * will be instantiated and handled here
 *
 * Extends [DaggerAppCompatActivity] for auto-injecting dependencies via Dagger2
 *
 * Note: Since this Activity holds the responsibility of being a container,
 * it will be kept as MVP-free for over-engineering concerns. Make sure to keep it free from heavy logic operations
 * and keep the intent as simple as changing fragments/views and editing common views. Business should be handled via fragments.
 *
 */
class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Change fragment to ProductFragment at the start of the application
        changeFragment(ProductFragment.newInstance(),false)
    }

    /**
     * Replaces current fragment, adds Fragment to back stack
     *
     * @param fragment fragment to be replaced
     * @param fullScreen determines if the fragment to be replaced should be at the bottom of the appbar
     * or placed on top
     *
     */
    fun changeFragment(fragment: Fragment, fullScreen: Boolean) {
        val fragmentManager = supportFragmentManager.beginTransaction()
        val fragmentContainer = if (fullScreen) R.id.frame_fullscreen else R.id.frame_under_toolbar
        fragmentManager.replace(fragmentContainer, fragment, fragment.javaClass.simpleName)
        fragmentManager.addToBackStack(fragment.javaClass.simpleName)
        fragmentManager.commit()
    }

    /**
     * Makes [R.layout.loading_layout] visible
     *
     * Changes progress bar color to [R.color.colorMCYellow]
     */
    fun showLoading() {
        loading.progress_bar.indeterminateDrawable.setColorFilter(
            resources.getColor(R.color.colorMCYellow),
            PorterDuff.Mode.SRC_IN
        )

        loading.visibility = View.VISIBLE
    }

    /**
     * Makes [R.layout.loading_layout] gone
     */
    fun hideLoading() {
        loading.visibility = View.GONE
    }

}
