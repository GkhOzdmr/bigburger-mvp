package com.bigburger.presentation.basket

import com.bigburger.data.model.BasketProductListEvent
import com.bigburger.data.model.BasketProductModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.lang.IndexOutOfBoundsException
import java.util.*

/**
 * [BasketContract.Presenter] implementation class
 *
 * @property basketProductList List of [BasketProductModel]
 * @property view [BasketContract.View] implementation instance see [BasketFragment]
 * @property disposable RxJava Subscriptions container
 */
class BasketPresenter : BasketContract.Presenter {

    private var view: BasketContract.View? = null
    private val disposable: CompositeDisposable = CompositeDisposable()
    private var basketProductList = mutableListOf<BasketProductModel>()

    override fun onCreate() {

    }

    override fun onDestroy() {
        dropView()
        disposeSubscriptions()
    }

    override fun takeView(view: BasketContract.View) {
        this.view = view
    }

    override fun dropView() {
        this.view = null
    }

    override fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

    /**
     * Replaces [basketProductList]
     *
     * @param basketProductList list of [BasketProductModel]
     */
    override fun setBasketProducts(basketProductList: MutableList<BasketProductModel>) {
        disposable.addAll(
            Observable.fromArray(basketProductList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    this.basketProductList = it
                    view!!.populateRecyclerView(this.basketProductList)
                    calculateSubtotal()
                    Timber.d("basketProductList.size: ${it.size}")
                },{},{})
        )
    }

    override fun getBasketProducts(): MutableList<BasketProductModel> {
        return this.basketProductList
    }

    /**
     * Calculates subtotal of all products & notifies view with the value
     */
    private fun calculateSubtotal() {
        var subtotal = 0L
        disposable.addAll(
            Observable.fromIterable(basketProductList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    subtotal += it.amount * it.productModel!!.price
                }.doOnTerminate {
                    view!!.updateSubtotal(subtotal)
                }.subscribe()
        )
    }

    /**
     * Deletes the item from list, calculates subtotal and updates view
     *
     * @param position index of list where the delete happen
     */
    override fun deleteItemAt(position: Int) {
        Timber.d("pre list size: ${basketProductList.size} position: $position")
        // Delete item at position
        basketProductList.removeAt(position)
        Timber.d("post list size: ${basketProductList.size}")
        // Calculate new price
        calculateSubtotal()
        // Notifying listeners with new list
        EventBus.getDefault().post(BasketProductListEvent(basketProductList))
    }

    /**
     * Updates the product amount of item at index
     *
     * @param position index of item at list
     * @param amount new amount to be set
     */
    override fun updateForAmountChange(position: Int, amount: Int) {
        Timber.d("basketProductList.size ${basketProductList.size} position: $position")
        try {
            basketProductList[position].amount = amount
        } catch (e: IndexOutOfBoundsException) {

        }
        calculateSubtotal()
    }

}