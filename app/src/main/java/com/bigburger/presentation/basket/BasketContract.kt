package com.bigburger.presentation.basket

import com.bigburger.base.BasePresenter
import com.bigburger.base.BaseView
import com.bigburger.data.model.BasketProductModel
import com.bigburger.presentation.productorder.ProductOrderContract

interface BasketContract {

    interface View : BaseView<ProductOrderContract.Presenter> {
        fun populateRecyclerView(basketProductList: MutableList<BasketProductModel>)
        fun updateSubtotal(subtotal: Long)
    }

    interface Presenter : BasePresenter<View> {
        fun setBasketProducts(basketProductList: MutableList<BasketProductModel>)
        fun getBasketProducts(): MutableList<BasketProductModel>
        fun deleteItemAt(position: Int)
        fun updateForAmountChange(position: Int, amount: Int)
    }

}