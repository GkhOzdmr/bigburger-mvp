package com.bigburger.presentation.basket.adapter

import android.content.Context
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bigburger.R
import com.bigburger.common.GlideUtil
import com.bigburger.common.PriceUtil
import com.bigburger.data.model.BasketProductModel
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_basket.view.*
import timber.log.Timber
import java.lang.NumberFormatException
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

//TODO Documentation and Unit Tests

/**
 * RecyclerView adapter class for displaying [BasketProductModel]
 *
 *
 * @property context Context at which this adapter instance created
 * @property notifier [Notifier] implementation for notifying recycler view actions
 * @property items list of [BasketProductModel]
 */
class BasketAdapter(
    private val context: Context,
    private val items: MutableList<BasketProductModel>,
    private val notifier: Notifier
) : RecyclerView.Adapter<BasketAdapter.ViewHolder>() {

    /**
     * An interface for notifying implementors for recycler view actions
     */
    interface Notifier {
        fun deleteButtonClick(position: Int)
        fun notifyAmountChange(position: Int, amount: Int)
    }

    private val disposable = CompositeDisposable()
//    private var items = mutableListOf<BasketProductModel>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Basic identification
        val product = items[position].productModel!!
        var productAmount = items[position].amount
        val productIngredients = items[position].ingredients

        // Sets the product name
        holder.tv_product_name.text = product.title

        // Displays product image
        GlideUtil.displayImage(Uri.parse(product.thumbnail), null, context, holder.img_product)

        // Displays product amount
        holder.edt_product_amount.setText(productAmount.toString())

        // Displays product total price
        fun totalPrice() {
            val totalPrice = productAmount * product.price
            holder.tv_product_total_price.text = PriceUtil.priceWithSymbol(context, totalPrice)
        }
        totalPrice()

        // Display product ingredients if not empty
        holder.tv_product_ingredients.text =
            if (productIngredients != "") context.resources.getString(R.string.ingredients, productIngredients)
            else ""

        // Text listener to change product amount via. RxBindings
        //      Purpose:
        // Listens for input on edit text for amount change
        // Whenever input is empty amount is set to "1"
        // This is due observer throwing NumberFormatException, if error is thrown,
        // Subscription will die and no longer keep listening inputs.
        // Feel free to change if there is better way.
        //
        // At the end of the subscription, updates will happen for amount & Subtotal change.
        disposable.add(
            holder.edt_product_amount.textChanges()
                .switchMap { charSequence: CharSequence ->
                    var amount = 1
                    try {
                        amount = charSequence.toString().toInt()
                        Timber.d("amount: $amount")
                    } catch (t: NumberFormatException) {
                        amount = 1
                    }

                    Observable.defer{ Observable.just(amount).subscribeOn(Schedulers.io()) }.subscribeOn(Schedulers.io())
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete {

                }
                .subscribe {
                    productAmount = it
                    totalPrice()
                    //Updates for amount change so that the subtotal can be updated
                    notifier.notifyAmountChange(position, it)
                }
        )

//        holder.edt_product_amount.addTextChangedListener(object: TextWatcher{
//            override fun afterTextChanged(s: Editable?) {
//
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                var amountText = if(s.toString()!="") s.toString().toInt() else 1
//                if (amountText != 0) {
//                    productAmount = amountText
//                    totalPrice()
//                    //Updates for amount change so that the subtotal can be updated
//                    notifier.notifyAmountChange(position, amountText)
//                }
//            }
//        })

        //Clicking img_delete notifies implementors and deletes the respective item from recycler view
        holder.img_delete.setOnClickListener {
            Observable.just(notifier.deleteButtonClick(position))
                .subscribe {
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                }
        }

    }

    /**
     * Replaces items of this adapter with given list of [BasketProductModel] and
     * updates recycler view
     *
     * @param basketProductList list of [BasketProductModel]
     */
//    fun updateAdapterItems(basketProductList: MutableList<BasketProductModel>) {
//        this.items = basketProductList
//        notifyDataSetChanged()
//    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img_product: ImageView = view.img_product
        val tv_product_name: TextView = view.tv_product_name
        val tv_product_ingredients: TextView = view.tv_product_ingredients
        val edt_product_amount: EditText = view.edt_product_amount
        val tv_product_total_price: TextView = view.tv_product_total_price
        val img_delete: ImageView = view.img_delete
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_basket, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

}