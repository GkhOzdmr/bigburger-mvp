package com.bigburger.presentation.basket.fragment

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import com.bigburger.R
import com.bigburger.common.PriceUtil
import com.bigburger.data.model.BasketProductListEvent
import com.bigburger.data.model.BasketProductModel
import com.bigburger.presentation.basket.BasketContract
import com.bigburger.presentation.basket.adapter.BasketAdapter
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_basket.*
import org.greenrobot.eventbus.EventBus
import java.util.ArrayList
import javax.inject.Inject


/**
 * Argument constants to be used in [BasketFragment]
 */
private const val ARG_BASKET_PRODUCT_LIST = "basketProductModel"


/**
 * [Fragment] implementation.
 * Use the [BasketFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 * Used to display and checkout Products.
 *
*/
class BasketFragment : DaggerFragment(), BasketContract.View, BasketAdapter.Notifier {

    @Inject
    lateinit var presenter: BasketContract.Presenter
    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        presenter.takeView(this)
        return inflater.inflate(R.layout.fragment_basket, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            presenter.setBasketProducts(it.getParcelableArrayList<BasketProductModel>(ARG_BASKET_PRODUCT_LIST))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDestroy()
        disposeSubscriptions()
    }

    /**
     * Disposes subscriptions
     */
    private fun disposeSubscriptions() {
        if(!disposable.isDisposed) disposable.dispose()
    }

    /**
     * Displays given list of [BasketProductModel] on on recycler view
     *
     * @param basketProductList list of [BasketProductModel]
     */
    override fun populateRecyclerView(basketProductList: MutableList<BasketProductModel>) {
        if (basketProductList.isNotEmpty()) {
            rc_basket.apply {
                layoutManager = LinearLayoutManager(context)

                val dividerItemDecoration = DividerItemDecoration(context,  LinearLayout.VERTICAL)
                dividerItemDecoration.setDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.divider_view,
                        null
                    )!!
                )

                rc_basket.addItemDecoration(dividerItemDecoration)
                adapter = BasketAdapter(context!!, basketProductList,this@BasketFragment)

            }
        } else {
            rc_basket.visibility = View.GONE
            tv_cart_is_empty.visibility = View.VISIBLE
        }
    }

    /**
     * Converts price in long value to string and updates the view
     *
     * @param subtotal price in [Long]
     */
    override fun updateSubtotal(subtotal: Long) {
        tv_subtotal.text = PriceUtil.priceWithSymbol(context!!, subtotal)
    }

    /**
     * This function is overridden from BasketAdapter.Notifier
     *
     * Deletes the item from list at presenter & updates the list for [ProductFragment]
     *
     * @param position item position in list
     */
    override fun deleteButtonClick(position: Int) {
        // Deletes the item from list at presenter
        presenter.deleteItemAt(position)
    }

    /**
     * This function is overridden from BasketAdapter.Notifier
     *
     * Notifies the item amount change
     *
     * @param position item position that change happened
     * @param amount new amount
     */
    override fun notifyAmountChange(position: Int, amount: Int) {
        presenter.updateForAmountChange(position, amount)
    }

    companion object {

        /**
         * Creates new instance of [BasketFragment] with arguments
         *
         * @param listOfBasketProduct list of parcelable [BasketProductModel] to be used in fragment
         */
        @JvmStatic
        fun newInstance(listOfBasketProduct: List<BasketProductModel>) =
            BasketFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(ARG_BASKET_PRODUCT_LIST, listOfBasketProduct as ArrayList<out Parcelable>)
                }
            }
    }
}
