package com.bigburger.di

import com.bigburger.data.remote.VibuyApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

/**
 * Dagger2 Module for providing local & remote Service instances
 */
@Module
class ServiceModule {


    /**
     * Provides instance of [VibuyApi]
     *
     * @param retrofit Retrofit instance with @Named(VIBUY_API) annotation
     * @return Vibuy Retrofit API instance
     */
    @ActivityScope
    @Provides
    @Named(SERVICE_VIBUY)
    fun provideVibuyApi(@Named(API_VIBUY) retrofit: Retrofit): VibuyApi {
        return retrofit.create(VibuyApi::class.java)
    }

}