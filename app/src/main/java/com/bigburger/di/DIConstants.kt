package com.bigburger.di

/**
 * Constant values to be used in Dagger2 @Named annotations
 */

const val SERVICE_VIBUY = "VIBUY_SERVICE"
const val API_VIBUY = "VIBUY_API"
const val SCHDULER_MAIN = "MAIN_THREAD"
const val SCHDULER_IO = "IO_THREAD"