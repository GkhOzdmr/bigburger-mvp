package com.bigburger.di

import android.content.Context
import com.bigburger.base.BigBurgerApp
import dagger.Binds
import dagger.Module

/**
 * Dagger2 Module for providing Application wise dependencies
 */
@Module
abstract class AppModule{

    /**
     * Provides context for modules that need context of the application
     *
     * @return Application context
     */
    @Binds
    abstract fun provideContext(application: BigBurgerApp): Context

}
