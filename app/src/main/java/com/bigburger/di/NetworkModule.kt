package com.bigburger.di

import android.annotation.TargetApi
import com.bigburger.data.remote.NetworkConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

/**
 * Dagger2 Module for providing Network tools instances
 */
@Module
class NetworkModule {

    /**
     * Provides Retrofit instance for VIBUY_API
     *
     * Base URL: [NetworkConfig.VIBUY_BASE]
     *
     * @param gson GSon serializer
     * @param okHttpClient OkHttpClient of OkHttp3
     * @return Instance of Retrofit with @Named(VIBUY_API) annotation
     */
    @ActivityScope
    @Provides
    @Named(API_VIBUY)
    fun provideRetrofitForVibuyApi(gson: Gson, okHttpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(NetworkConfig.VIBUY_BASE)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    /**
     *
     * Provides GSon instance
     *
     * @return gson instance
     */
    @ActivityScope
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().setLenient().create()
    }

    /**
     *
     * Provides OkHttpClient with logging interceptor
     * You can check api activity from logs with "OkHttp" tag
     *
     * Timeouts for api connections can be set here(Use java.util.concurrent.TimeUnit version of TimeUnit)
     *
     * @return OkHttpClient instance
     */
    @ActivityScope
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val loginInterceptor = HttpLoggingInterceptor()
        loginInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .connectTimeout(30000, TimeUnit.MILLISECONDS)
            .readTimeout(30000, TimeUnit.MILLISECONDS)
            .writeTimeout(30000, TimeUnit.MILLISECONDS)
            .addInterceptor(loginInterceptor)
            .build()
    }

}