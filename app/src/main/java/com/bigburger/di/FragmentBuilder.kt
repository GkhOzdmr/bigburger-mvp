package com.bigburger.di

import com.bigburger.presentation.basket.fragment.BasketFragment
import com.bigburger.presentation.product.fragment.ProductFragment
import com.bigburger.presentation.productorder.fragment.ProductOrderFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlin.reflect.KClass

/**
 * Dagger2 Module for providing Fragment instances
 */
@Module
abstract class FragmentBuilder {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            PresenterModule::class,
            RepositoryModule::class,
            ServiceModule::class
        ]
    )
    internal abstract fun bindProductsFragment(): ProductFragment

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            PresenterModule::class,
            RepositoryModule::class,
            ServiceModule::class
        ]
    )
    internal abstract fun bindProductOrderFragment(): ProductOrderFragment

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            PresenterModule::class,
            RepositoryModule::class,
            ServiceModule::class
        ]
    )
    internal abstract fun bindBasketFragment(): BasketFragment

}