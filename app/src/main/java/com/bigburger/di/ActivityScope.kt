package com.bigburger.di

import javax.inject.Scope

/**
 * Dagger2 Scope for Application Activities
 */
@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope