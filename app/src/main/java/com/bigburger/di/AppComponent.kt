package com.bigburger.di

import com.bigburger.base.BigBurgerApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Dagger2 Component for injecting modules to application
 */
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuilder::class,
    FragmentBuilder::class,
    AppModule::class
])
interface AppComponent : AndroidInjector<BigBurgerApp> {

    /**
     * Injects dependencies with declared modules
     */
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: BigBurgerApp) : Builder
        fun build() : AppComponent
    }

  //  override fun inject(application: BigBurgerApp)
}