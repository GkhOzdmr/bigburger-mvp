package com.bigburger.di

import com.bigburger.presentation.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Dagger2 Activity injector
 *
 * Modules that activities use can be injected here
 */
@Module
abstract class ActivityBuilder {

    /**
     * Injects given modules to MainActivity
     */
    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            PresenterModule::class,
            RepositoryModule::class,
            ServiceModule::class
        ]
    )
    internal abstract fun bindMainActivity(): MainActivity

}