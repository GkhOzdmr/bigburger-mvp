package com.bigburger.di

import com.bigburger.data.remote.VibuyApi
import com.bigburger.data.repository.ProductOrderRepository
import com.bigburger.data.repository.ProductOrderRepositoryImpl
import com.bigburger.data.repository.ProductRepository
import com.bigburger.data.repository.ProductRepositoryImpl
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

/**
 * Dagger2 Module for providing repository interface implementor instances
 */
@Module
class RepositoryModule {

    /**
     * Provides RemoteRepository implementation
     *
     * @param vibuyApi [VibuyApi] instance
     * @return returns RemoteRepository interface implementation with given parameters
     */
    @ActivityScope
    @Provides
    fun provideProductRepository(@Named(SERVICE_VIBUY) vibuyApi: VibuyApi): ProductRepository =
        ProductRepositoryImpl(vibuyApi)


    /**
     * Provides ProductOrderRepository implementation
     */
    @ActivityScope
    @Provides
    fun provideProductOrderRepository(): ProductOrderRepository = ProductOrderRepositoryImpl()

}

