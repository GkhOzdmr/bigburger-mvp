package com.bigburger.di

import com.bigburger.data.repository.ProductOrderRepository
import com.bigburger.data.repository.ProductRepository
import com.bigburger.presentation.basket.BasketContract
import com.bigburger.presentation.basket.BasketPresenter
import com.bigburger.presentation.product.ProductContract
import com.bigburger.presentation.product.ProductPresenter
import com.bigburger.presentation.productorder.ProductOrderContract
import com.bigburger.presentation.productorder.ProductOrderPresenter
import dagger.Module
import dagger.Provides

/**
 * Dagger2 Module for providing Presenter(MVP) instances
 */
@Module
class PresenterModule {

    //TODO: Document functions below

    @Provides
    @ActivityScope
    fun provideProductPresenter(productRepository: ProductRepository): ProductContract.Presenter =
            ProductPresenter(productRepository)

    @Provides
    @ActivityScope
    fun provideProductOrderPresenter(productOrderRepository: ProductOrderRepository): ProductOrderContract.Presenter =
        ProductOrderPresenter(productOrderRepository)

    @Provides
    @ActivityScope
    fun provideBasketPresenter(): BasketContract.Presenter =
            BasketPresenter()


}