package com.bigburger.base

/**
 * Base Presenter that every specific Presenter should implement
 *
 * @param T the view associated with this presenter
 */
interface BasePresenter<V> {

    /**
     * Binds presenter with a view when resumed. The Presenter will perform initialization here.
     *
     * @param view the view associated with this presenter (taken from type parameter)
     */
    fun takeView(view: V)

    /**
     * Drops the reference to the view when destroyed
     */
    fun dropView()


    /**
     * Function to be called at the create/start state of the View's implementation
     */
    fun onCreate()


    /**
     * Function to be called at destroy/stop state of the View's implementation
     */
    fun onDestroy()

    /**
     * Disposes subscriptions, should be called at onStop/onDestroy state
     */
    fun disposeSubscriptions()

}