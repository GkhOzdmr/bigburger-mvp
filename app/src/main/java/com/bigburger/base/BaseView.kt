package com.bigburger.base

/**
 *
 * Base View that every specific View should implement
 *
 */
interface BaseView<P>