package com.bigburger.base

import android.content.Context
import androidx.multidex.MultiDex
import com.bigburger.BuildConfig
import com.bigburger.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber

/**
 *
 * This application's [DaggerApplication] implementation
 *
 */
class BigBurgerApp : DaggerApplication() {

    /**
     * Dagger2 injector plantation
     *
     * @see com.bigburger.di.ActivityBuilder
     * @see com.bigburger.di.FragmentBuilder
     */
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }

    /**
     *
     * This method gets called at the creation of application.
     *
     * Contains:
     *   - [Timber] plantation for DEBUG version of the application.
     *
     */
    override fun onCreate() {
        super.onCreate()

        //Timber plantation. On release version, Timber logs won't be compiled.
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

}