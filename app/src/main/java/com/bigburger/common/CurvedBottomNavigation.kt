package com.bigburger.common

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.bigburger.R
import com.google.android.material.bottomnavigation.BottomNavigationView

//TODO document this class
class CurvedBottomNavigation : BottomNavigationView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context,attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAtr:Int) :  super(context,attrs,defStyleAtr)

    private val path = Path()
    private val paint = Paint()

    private val CURVE_CIRCLE_RADIUS = 140 / 2

    private var firstCurveStartPoint = Point()
    private var firstCurveEndPoint = Point()
    private var firstCurveControlPoint1 = Point()
    private var firstCurveControlPoint2 = Point()

    private var secondCurveStartPoint = Point()
    private var secondCurveEndPoint = Point()
    private var secondCurveControlPoint1 = Point()
    private var secondCurveControlPoint2 = Point()

    private var bottomNavWidth = 0
    private var bottomNavHeight = 0

    init {
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.color = ContextCompat.getColor(context, R.color.colorMCRed)
        setBackgroundColor(Color.TRANSPARENT)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        bottomNavWidth = width
        bottomNavHeight = height

        firstCurveStartPoint.set(
            (bottomNavWidth / 2) - (CURVE_CIRCLE_RADIUS * 2) - (CURVE_CIRCLE_RADIUS / 3), 0
        )

        firstCurveEndPoint.set(bottomNavWidth / 2, CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4))

        secondCurveStartPoint = firstCurveEndPoint

        secondCurveEndPoint.set(
            (bottomNavWidth / 2) + (CURVE_CIRCLE_RADIUS * 2) + (CURVE_CIRCLE_RADIUS / 3), 0
        )

        firstCurveControlPoint1.set(
            firstCurveStartPoint.x + CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4),
            firstCurveStartPoint.y
        )

        firstCurveControlPoint2.set(
            firstCurveEndPoint.x - (CURVE_CIRCLE_RADIUS * 2) + CURVE_CIRCLE_RADIUS, firstCurveEndPoint.y
        )

        secondCurveControlPoint1.set(
            secondCurveStartPoint.x + (CURVE_CIRCLE_RADIUS * 2) - CURVE_CIRCLE_RADIUS, secondCurveStartPoint.y
        )

        secondCurveControlPoint2.set(
            secondCurveEndPoint.x - (CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4)), secondCurveEndPoint.y
        )

        path.reset()
        path.moveTo(0f,0f)
        path.lineTo(firstCurveStartPoint.x.toFloat(), firstCurveStartPoint.y.toFloat())

        path.cubicTo(
            firstCurveControlPoint1.x.toFloat(), firstCurveControlPoint1.y.toFloat(),
            firstCurveControlPoint2.x.toFloat(), firstCurveControlPoint2.y.toFloat(),
            firstCurveEndPoint.x.toFloat(), firstCurveEndPoint.y.toFloat()
        )

        path.cubicTo(
            secondCurveControlPoint1.x.toFloat(), secondCurveControlPoint1.y.toFloat(),
            secondCurveControlPoint2.x.toFloat(), secondCurveControlPoint2.y.toFloat(),
            secondCurveEndPoint.x.toFloat(), secondCurveEndPoint.y.toFloat()
        )

        path.lineTo(bottomNavWidth.toFloat(), 0f)
        path.lineTo(bottomNavWidth.toFloat(), bottomNavHeight.toFloat())
        path.lineTo(0f, bottomNavHeight.toFloat())
        path.close()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawPath(path,paint)
    }
}