package com.bigburger.common

/**
 * A common interface that's any Error type classes should implement.
 *
 * Used for getting Error messages, see [ErrorMessage]
 *
 * Implementors:
 * [com.bigburger.data.remote.NetworkErrorType]
 *
 */
interface ErrorType