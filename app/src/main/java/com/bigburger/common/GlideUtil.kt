package com.bigburger.common

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.TypedValue
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bigburger.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition

object GlideUtil {

    /**
     * Displays item from either Uri or Drawable with default error handling & drawable with the help of Glide library
     *
     * @param imgUri Nullable image Uri
     * @param imgDrawable Nullable image drawable
     * @param context context of the application
     * @param container ImageView in which the image will be displayed at
     *
     * @exception Exception Couldn't find single source of truth for image.
     * Throws whenever both source is null or not-null at the same time
     *
     */
    fun displayImage(imgUri: Uri?, imgDrawable: DrawableRes?, context: Context, container: ImageView) {
        if (imgUri == null && imgDrawable == null) throw Exception("Both Uri and Drawable is null. You have to specify one source for image.")
        if (imgUri != null && imgDrawable != null) throw Exception("Both Uri and Drawable is not-null. You have to specify one source for image.")

        val imageSource = imgUri ?: imgDrawable

        Glide.with(context)
            .asBitmap()
            .load(imageSource)
            .placeholder(R.drawable.image_not_available)
            .error(R.drawable.image_not_available)
            .listener(object : RequestListener<Bitmap> {
                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?, model: Any?,
                    target: com.bumptech.glide.request.target.Target<Bitmap>?, isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(container)
    }

    /**
     * Downloads image and turns it into Bitmap. Then, special function make Bitmap's corner rounded
     * and loads into given ImageView. When image cannot be downloaded, a default will be loaded.
     *
     * @param imgUri image Uri
     * @param context context at call site
     * @param container ImageView for displaying image
     */
    fun displayImageRoundCornered(imgUri: Uri, context: Context, container: ImageView) {
        Glide.with(context)
            .asBitmap()
            .placeholder(R.drawable.image_not_available)
            .error(R.drawable.image_not_available)
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

            })
            .load(imgUri)
            // This is after image downloaded and transformed into Bitmap,
            // Bitmap's corners will be rounded (8dp) and then Bitmap will be
            // loaded into given ImageView
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {
                }

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val dpToPx = TypedValue
                        .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8F, context.resources.displayMetrics).toInt()

                    Glide.with(context)
                        .load(ImageHelper.getRoundedCornerBitmap(resource, dpToPx))
                        .into(container)
                }

            })
    }

}