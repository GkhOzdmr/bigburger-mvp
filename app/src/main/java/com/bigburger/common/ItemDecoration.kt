package com.bigburger.common

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Item decoration class to be used with adapters that uses GridLayoutManager
 *
 * @param top Sets the margin of top
 * @param bottom Sets the margin of bottom
 * @param start Sets the margin of start
 * @param end Sets the margin of end
 */
class ItemDecoration(
    val top :Int,
    val bottom :Int,
    val start :Int,
    val end :Int
) : RecyclerView.ItemDecoration() {

    /**
     * Sets the item margins to values that has been given at the constructor of this class
     */
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.top = this.top
        outRect.bottom = this.bottom
        outRect.right = this.start
        outRect.left = this.end
    }
}