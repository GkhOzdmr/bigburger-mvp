package com.bigburger.common

/**
 * Sealed class to be used with EventBus
 *
 * Contains simple types to be used with EventBus to notify listeners about particular job.
 */
sealed class BasicEvent {

    /**
     * Used whenever Try again buttons/functions wished to be triggered.
     */
    object TryAgain: BasicEvent()
}