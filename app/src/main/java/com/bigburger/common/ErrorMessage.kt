package com.bigburger.common

import android.content.Context
import com.bigburger.R
import com.bigburger.data.model.ErrorModel
import com.bigburger.data.remote.NetworkErrorType

/**
 * Object class for getting error messages
 */
object ErrorMessage {

    /**
     * Returns [ErrorModel] that contains error header and message that's matching to [ErrorType]
     *
     * @param context app context
     * @param errorType ErrorType interface implementor instance
     */
    fun getMessage(context: Context, errorType: ErrorType): ErrorModel {
        var errorHeader = ""
        var errorMessage = ""

        when (errorType) {
            NetworkErrorType.NoConnection -> {
                errorHeader = context.resources.getString(R.string.error_no_connection_header)
                errorMessage = context.resources.getString(R.string.error_no_connection_message)
            }

            NetworkErrorType.UnknownError -> {
                errorHeader = context.resources.getString(R.string.error_unknown_header)
                errorMessage = context.resources.getString(R.string.error_unknown_message)
            }
        }

        return ErrorModel(errorHeader, errorMessage)
    }

}