package com.bigburger.common

import android.content.Context
import com.bigburger.R

/**
 * Common tools for retrieving/converting Price values etc.
 */
object PriceUtil {

    /**
     *
     * This functions is designed for VIBUY_API returning [com.bigburger.data.model.ProductModel]
     *
     * ProductModel has a field called price which is a non-formatted version of currency TL
     *
     * What this functions does is, formats the given price value so that it is decimalized
     * starting from the second last index of the value(right to left) (ie. Price in long = 14945L,
     * formatted version= "149.45")
     *
     * @param rawPrice Price in long format to be decimalized
     *
     * @return string that is decimalized version of long
     */
    fun rawPriceToString(rawPrice: Long): String {
        val lenghtOfLong = rawPrice.toString().length

        return if (lenghtOfLong > 2) {
            val lastIndex = lenghtOfLong - 1
            val decimalizedStr = rawPrice.toString().substring(0, lastIndex - 1) +
                    '.' + rawPrice.toString().substring(lastIndex - 1, lastIndex + 1)

            decimalizedStr

        } else {
            rawPrice.toString()
        }
    }

    /**
     * Uses rawPriceToString to decimalize price and adds symbol according to decimalization.
     *
     * @param context context at call site
     * @param rawPrice price as long
     *
     * @return decimalized price with symbol added
     */
    fun priceWithSymbol(context: Context, rawPrice: Long): String {
        val rawPriceString = rawPriceToString(rawPrice)
        return if (rawPriceString.length > 2) {
            context.resources.getString(R.string.price_in_dollar, rawPriceString)
        } else {
            context.resources.getString(R.string.price_in_cent, rawPriceString)
        }
    }

}