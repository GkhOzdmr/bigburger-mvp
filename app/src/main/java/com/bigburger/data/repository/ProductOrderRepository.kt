package com.bigburger.data.repository

import com.bigburger.data.local.IngredientType
import com.bigburger.data.model.IngredientModel
import io.reactivex.Observable
import io.reactivex.Single

/**
 * RepositoryPattern interface for [ProductOrderFragment]
 */
interface ProductOrderRepository {

    fun getIngredients(ingredientType: IngredientType): Observable<List<IngredientModel>>

}