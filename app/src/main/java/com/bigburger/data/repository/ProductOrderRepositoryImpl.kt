package com.bigburger.data.repository

import com.bigburger.data.local.IngredientRepo
import com.bigburger.data.local.IngredientType
import com.bigburger.data.model.IngredientModel
import io.reactivex.Observable
import io.reactivex.Single

/**
 * [ProductOrderRepository] implementation
 *
 * Combines data sources and returns data
 */
class ProductOrderRepositoryImpl : ProductOrderRepository {

    /**
     * Returns stream of ingredients respective to given [IngredientType]
     *
     * @return Observable list of [IngredientModel]
     */
    override fun getIngredients(ingredientType: IngredientType): Observable<List<IngredientModel>> {
        return if (ingredientType == IngredientType.HamburgerIngredients) {
            IngredientRepo.foodIngredients
        } else {
            IngredientRepo.drinkIngredients
        }
    }

}