package com.bigburger.data.repository

import com.bigburger.data.model.ProductModel
import com.bigburger.data.remote.VibuyApi
import io.reactivex.Scheduler
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

/**
 * Repository class that implements [ProductRepository]
 *
 *@param vibuyApi Retrofit interface to make http calls to VIBUY API
 */
class ProductRepositoryImpl @Inject constructor(private val vibuyApi: VibuyApi) : ProductRepository {

    /**
     * Does API call to return a list of [ProductModel]
     *
     * @return Observable of type [Single] from List of [ProductModel]
     */
    override fun getProducts(): Single<Response<List<ProductModel>>> {
        return vibuyApi.getProducts()
    }

}