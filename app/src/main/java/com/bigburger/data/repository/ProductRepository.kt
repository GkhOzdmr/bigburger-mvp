package com.bigburger.data.repository

import com.bigburger.data.model.ProductModel
import io.reactivex.Single
import retrofit2.Response

/**
 * Repository pattern interface for Products
 *
 * @see com.bigburger.data.remote.VibuyApi
 * @see com.bigburger.data.model.ProductModel
 */
interface ProductRepository {

    /**
     * Gets list of product from either local or remote sources or both combined
     * depending on network state or cache date validation.
     *
     * Note: In this case local caching did NOT implemented,
     * products will be fetched from remote as fresh
     *
     * @return Observable [List] of [ProductModel] wrapped to [Response]
     */
    fun getProducts(): Single<Response<List<ProductModel>>>

}