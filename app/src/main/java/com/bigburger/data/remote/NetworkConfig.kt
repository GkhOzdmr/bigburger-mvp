package com.bigburger.data.remote

/**
 * Kotlin singleton Object for API links, keys & constants
 */
object NetworkConfig {

    /**
     * Main link of Vibuy API
     */
    const val VIBUY_BASE = "http://legacy.vibuy.com/dump/"

    /**
     * Sub link that returns list of Products from Vibuy API in JSON format(REST API)
     *
     * Base URL = [NetworkConfig.VIBUY_BASE]
     *
     * Model Class = [com.bigburger.data.model.ProductModel]
     *
     * Retrofit Usage = [VibuyApi.getProducts]
     */
    const val VIBUY_PRODUCTS = "mobiletest1.json"
}