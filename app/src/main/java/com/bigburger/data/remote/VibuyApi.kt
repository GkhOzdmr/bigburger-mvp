package com.bigburger.data.remote

import com.bigburger.data.model.ProductModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

/**
 * Interface for creating Retrofit API calls of Vibuy API
 */
interface VibuyApi{

    /**
     *
     * @return List of [ProductModel]
     *
     * See [NetworkConfig.VIBUY_PRODUCTS] for dependencies and further documentation
     *
     */
    @GET(NetworkConfig.VIBUY_PRODUCTS)
    fun getProducts() : Single<Response<List<ProductModel>>>

}