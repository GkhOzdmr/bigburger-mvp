package com.bigburger.data.remote

import com.bigburger.common.ErrorType

/**
 * Sealed class for declaring Error type from Web Service connection.
 */
sealed class NetworkErrorType: ErrorType {

    /**
     * Whenever connection to Network cannot be established, this type should be used.
     */
    object NoConnection: NetworkErrorType()

    /**
     * Whenever an Exception or Web Service Error type is not handled specifically, this type should be used.
     */
    object UnknownError: NetworkErrorType()

}