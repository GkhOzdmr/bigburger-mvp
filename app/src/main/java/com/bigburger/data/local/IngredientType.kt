package com.bigburger.data.local

/**
 * Sealed class for determining [IngredientType]
 */
sealed class IngredientType {
    object HamburgerIngredients : IngredientType()
    object DrinkIngredients : IngredientType()
}