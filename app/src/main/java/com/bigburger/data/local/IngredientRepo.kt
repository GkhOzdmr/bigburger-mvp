package com.bigburger.data.local

import com.bigburger.data.model.IngredientModel
import io.reactivex.Observable

/**
 * Temporary reposition solution for [IngredientModel]
 */
object IngredientRepo {

    val foodIngredients: Observable<List<IngredientModel>> = Observable.fromArray(
        listOf<IngredientModel>(
            IngredientModel("Tomato"),
            IngredientModel("Pickle"),
            IngredientModel("Onion"),
            IngredientModel("Ketchup"),
            IngredientModel("Mayonnaise")
        )
    )

    val drinkIngredients: Observable<List<IngredientModel>> = Observable.fromArray(
        listOf<IngredientModel>(
            IngredientModel("Extra ice"),
            IngredientModel("Lemon")
        )
    )

}