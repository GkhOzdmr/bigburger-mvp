package com.bigburger.data.model

/**
 * Data class for displaying ingredients
 *
 * @property ingredientName name of the ingredient
 */
data class IngredientModel(
    val ingredientName: String? = ""
){
    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}