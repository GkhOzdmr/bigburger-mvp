package com.bigburger.data.model

import android.os.Parcel
import android.os.Parcelable
import com.bigburger.data.remote.NetworkConfig
import com.google.gson.annotations.SerializedName

/**
 * Model class of the [com.bigburger.data.remote.NetworkConfig.VIBUY_PRODUCTS]
 *
 * See [NetworkConfig.VIBUY_PRODUCTS] for dependencies and further documentation
 */
data class ProductModel(
    @SerializedName("ref")
    val ref: String = "",

    @SerializedName("title")
    val title: String = "",

    @SerializedName("description")
    val description: String = "",

    @SerializedName("thumbnail")
    val thumbnail: String = "",

    @SerializedName("price")
    val price: Long = 0L
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ref)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(thumbnail)
        parcel.writeLong(price)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductModel> {
        override fun createFromParcel(parcel: Parcel): ProductModel {
            return ProductModel(parcel)
        }

        override fun newArray(size: Int): Array<ProductModel?> {
            return arrayOfNulls(size)
        }
    }

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}