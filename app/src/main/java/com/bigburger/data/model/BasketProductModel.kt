package com.bigburger.data.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Data class for displaying products at [BasketFragment]
 *
 * @property productModel product that's selected by user
 * @property ingredients user selected ingredients that belongs to this product
 * @property amount amount of products
 */
data class BasketProductModel(
    val productModel: ProductModel? = null,
    var ingredients: String? = "",
    var amount: Int = 0
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(ProductModel::class.java.classLoader),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(productModel, flags)
        parcel.writeString(ingredients)
        parcel.writeInt(amount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BasketProductModel> {
        override fun createFromParcel(parcel: Parcel): BasketProductModel {
            return BasketProductModel(parcel)
        }

        override fun newArray(size: Int): Array<BasketProductModel?> {
            return arrayOfNulls(size)
        }
    }

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}