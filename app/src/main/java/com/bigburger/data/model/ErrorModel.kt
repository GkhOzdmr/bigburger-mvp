package com.bigburger.data.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Data class for error strings
 *
 * @property errorHeader Header of the error
 * @property errorMessage Message of the error
 */
data class ErrorModel (
    val errorHeader: String? = "",
    val errorMessage: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(errorHeader)
        parcel.writeString(errorMessage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ErrorModel> {
        override fun createFromParcel(parcel: Parcel): ErrorModel {
            return ErrorModel(parcel)
        }

        override fun newArray(size: Int): Array<ErrorModel?> {
            return arrayOfNulls(size)
        }
    }
}