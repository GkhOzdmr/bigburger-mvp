package com.bigburger.data.model

/**
 * This class for EventBus.post()
 *
 * @param basketProducts list of [BasketProductModel]
 */
data class BasketProductListEvent(
    val basketProducts: List<BasketProductModel>
)