package com.bigburger

import org.junit.Test

class Statement2Solution {

    private val temperaturesList = listOf<Float>(
        -12F, -10F, -9.6F, -7.2F, -6.2F, -3.7F, -1.7F, 3.5F, 4F, 6.5F, 7F, 7F, 8F, 13F
    )

    /**
     *
     * Prints the value that is closest the zero.
     *
     * If the two value is the closest to zero and one is negative and other is positive,
     * positive value will be printed regardless of either its absolute value is bigger than the negative or not
     *
     */
    @Test
    fun `closestToZero should return nearest value to Zero`() {
        val value = closestToZero(temperaturesList)

        println(value)
    }

    /**
     * Sorts the given list of floats with insertions sort(assumed the list won't contain large amount of items)
     * Returns the most nearest positive value to zero
     *
     * @param temperatureList List of temperatures in type Float
     * @return positive number that is closest to zero
     */
    private fun closestToZero(temperatureList: List<Float>) :Float{
        var unSignedClosestToZero = 0F
        val sortedList = insertionSort(temperatureList)

        topLoop@for (item in sortedList) {
            if (item >= 0) {
                unSignedClosestToZero = item
                break@topLoop
            }
        }

        return unSignedClosestToZero
    }

    /**
     * Sorts the given list with ascending order
     *
     * @param input list of Float numbers to be sorted
     *
     * @return sorted list(ASC.) of given unsorted list
     */
    private  fun insertionSort(input: List<Float>): MutableList<Float> {
        val sortedList = input.toMutableList()
        for (i in 1 until sortedList.size) {
            val key = sortedList[i]
            var j = i - 1
            while (j >= 0 && sortedList[j] > key) {
                sortedList[j + 1] = sortedList[j]
                j = j - 1
            }
            sortedList[j + 1] = key
        }

        return sortedList
    }

}