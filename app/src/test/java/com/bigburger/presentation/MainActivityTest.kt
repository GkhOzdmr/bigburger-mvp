package com.bigburger.presentation

import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import com.bigburger.R
import com.bigburger.presentation.product.ProductPresenter
import com.bigburger.presentation.product.fragment.ProductFragment
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class MainActivityTest {

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `should navigate to ProductFragment at the start`() {
        // Given the MainActivity and the ProductFragment with mock Presenter
        val mainActivitySpy = Mockito.spy(MainActivity::class.java)
        val activityScenario = ActivityScenario.launch(MainActivity::class.java).moveToState(Lifecycle.State.STARTED)
        val productFragment = ProductFragment.newInstance().apply { this.presenter = mock(ProductPresenter::class.java) }

        activityScenario.onActivity {
            // When changeFragment called on onCreate of the MainActivity
            // with ProductFragment and false argument values
            verify(mainActivitySpy, times(1))
                .changeFragment(productFragment, false)

            // Then make sure ProductFragment is visible and the container is non fullScreen container
            val fragmentLayout = it.findViewById<View>(R.id.cl_fragment_products) as ConstraintLayout

            assertThat(fragmentLayout.visibility, `is`(equalTo(View.VISIBLE)))
            assertThat((fragmentLayout.parent as ViewGroup).id, `is`(equalTo(R.id.frame_under_toolbar)))
        }

    }

    @Test
    fun `changeFragment should replace current fragment`() {
        // Given the MainActivity and the ProductFragment with mock Presenter
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        val productFragment = ProductFragment.newInstance().apply {
            this.presenter = Mockito.mock(ProductPresenter::class.java)
        }

        activityScenario.onActivity {

            // When changeFragment called with a Fragment instance
            it.changeFragment(productFragment,fullScreen = false)

            // Then make sure the Fragment layout is displayed
            assertThat(
                it.findViewById<View>(R.id.cl_fragment_products).visibility,
                `is`(equalTo(View.VISIBLE))
            )
        }
    }

    @Test
    fun `showLoading should make loading_layout visible`() {
        // Given the MainActivity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        // When showLoading called on MainActivity
        activityScenario.onActivity {
            it.showLoading()

            assertThat(
                it.findViewById<View>(R.id.loading).visibility,
                `is`(equalTo(View.VISIBLE))
            )
        }
    }

    @Test
    fun `hideLoading should make loading_layout gone`() {
        // Given MainActivity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        activityScenario.onActivity {

            // When hideLoading called on MainActivity after the showLoading called
            it.showLoading()
            it.hideLoading()

            // Then make sure loading_layout is NOT visible
            assertThat(
                it.findViewById<View>(R.id.loading).visibility,
                `is`(equalTo(View.GONE))
            )
        }
    }

}