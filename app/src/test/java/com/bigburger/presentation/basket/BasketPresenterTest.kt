package com.bigburger.presentation.basket

import com.bigburger.MockRepo
import com.bigburger.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.verify
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BasketPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    lateinit var view: BasketContract.View

    private lateinit var basketPresenter: BasketContract.Presenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        basketPresenter = BasketPresenter()
        basketPresenter.takeView(view)
    }

    @After
    fun tearDown() {
        basketPresenter.onDestroy()
    }

    @Test
    fun setBasketProducts_CallsViewForDisplayingItems() {
        // When a method called with list of BasketProductModel
        basketPresenter.setBasketProducts(MockRepo.listOfBasketProductModel.toMutableList())

        // Then view called to display items
        verify(view).populateRecyclerView(any())
    }

    @Test
    fun updateForAmountChange_ReplacesAmountValueOfSpecifiedItem() {
        // Setup
        basketPresenter.setBasketProducts(MockRepo.listOfBasketProductModel.toMutableList())

        // When method called for amount change
        basketPresenter.updateForAmountChange(1, 9)

        // Then amount replaced with new value
        val EXPECTED_AMOUNT_VALUE = 9
        val ACTUAL_AMOUNT_VALUE = basketPresenter.getBasketProducts()[1].amount
        assertEquals(EXPECTED_AMOUNT_VALUE, ACTUAL_AMOUNT_VALUE)
    }
}