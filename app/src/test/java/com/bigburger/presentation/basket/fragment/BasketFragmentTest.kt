package com.bigburger.presentation.basket.fragment

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bigburger.MockRepo
import com.bigburger.R
import com.bigburger.TestUtils
import com.bigburger.data.model.BasketProductModel
import com.bigburger.presentation.MainActivity
import com.bigburger.presentation.basket.BasketContract
import kotlinx.android.synthetic.main.fragment_basket.view.*
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import java.util.ArrayList

@RunWith(RobolectricTestRunner::class)
class BasketFragmentTest {

    @Mock
    lateinit var presenter: BasketContract.Presenter

    private lateinit var mainActivity: MainActivity
    private lateinit var basketFragment: BasketFragment

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        mainActivity = Mockito.spy(MainActivity())
        basketFragment = BasketFragment.newInstance(MockRepo.arrayListOfBasketProductModel).apply {
            this.presenter = this@BasketFragmentTest.presenter
        }
    }

    @Test
    fun populateRecyclerView_WithEmptyList_ShowsNoItemsFoundState() {
        TestUtils.fragmentWithActivity(mainActivity, basketFragment) { activity, fragment ->
            val fra = fragment as BasketFragment
            fra.populateRecyclerView(emptyList<BasketProductModel>().toMutableList())

            val rc_basket = fra.view!!.findViewById<View>(R.id.rc_basket) as RecyclerView
            val tv_cart_is_empty = fra.view!!.findViewById<View>(R.id.tv_cart_is_empty) as TextView

            assertThat(rc_basket.visibility, `is`(equalTo(View.GONE)))
            assertThat(tv_cart_is_empty.visibility, `is`(equalTo(View.VISIBLE)))
        }
    }

    @Test
    fun updateSubtotal_WithPriceAsLong_DisplaysPriceWithSymbol() {
        TestUtils.fragmentWithActivity(mainActivity, basketFragment) { activity, fragment ->
            val fra = fragment as BasketFragment
            fra.updateSubtotal(900)

            val tv_subtotal = fra.view!!.findViewById<View>(R.id.tv_subtotal) as TextView
            val textViewString = tv_subtotal.text.toString()

            assertThat(textViewString, `is`(equalTo("9.00 TL")))
        }
    }

}