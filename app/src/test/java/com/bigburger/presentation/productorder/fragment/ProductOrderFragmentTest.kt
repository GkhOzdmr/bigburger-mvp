package com.bigburger.presentation.productorder.fragment

import androidx.test.core.app.ApplicationProvider
import com.bigburger.MockRepo
import com.bigburger.TestUtils
import com.bigburger.common.PriceUtil
import com.bigburger.data.model.ProductModel
import com.bigburger.presentation.MainActivity
import com.bigburger.presentation.productorder.ProductOrderContract
import com.bigburger.presentation.productorder.adapter.IngredientAdapter
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_product_order.*
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ProductOrderFragmentTest {

    @Mock
    private lateinit var mockProductOrderPresenter: ProductOrderContract.Presenter

    private lateinit var mainActivity: MainActivity
    private lateinit var productOrderFragment: ProductOrderFragment

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        mainActivity = spy(MainActivity())
        productOrderFragment = spy(
            ProductOrderFragment.newInstance(MockRepo.listOfProductModel[0]).apply {
                presenter = mockProductOrderPresenter
            }
        )
    }

    @Test
    fun `argument successfully delivered`() {
        TestUtils.fragmentWithActivity(mainActivity, productOrderFragment) { activity, fragment ->
            // When Fragment starts with the given Arguments
            val ARG_PRODUCT_MODEL = "productModel"

            // Then Fragment arguments is not null
            assertThat(fragment.arguments, `is`(notNullValue()))
            assertThat(fragment.arguments!!.getParcelable(ARG_PRODUCT_MODEL), `is`(notNullValue()))
            // And arguments integrity checked
            val productModel = fragment.arguments!!.getParcelable(ARG_PRODUCT_MODEL) as ProductModel
            assertThat(productModel.title, `is`(MockRepo.listOfProductModel[0].title))
            assertThat(productModel.description, `is`(MockRepo.listOfProductModel[0].description))
            assertThat(productModel.price, `is`(MockRepo.listOfProductModel[0].price))
            assertThat(productModel.ref, `is`(MockRepo.listOfProductModel[0].ref))
            assertThat(productModel.thumbnail, `is`(MockRepo.listOfProductModel[0].thumbnail))
        }
    }

    @Test
    fun `product details displayed`() {
        TestUtils.fragmentWithActivity(mainActivity,productOrderFragment) { activity, fragment ->
            val product = MockRepo.listOfProductModel[0]

            (fragment as ProductOrderFragment).displayProduct(product)

            assertThat(fragment.tv_product_name.text.toString(), `is`(product.title))
            assertThat(fragment.tv_product_price.text.toString(),`is`(PriceUtil.priceWithSymbol(ApplicationProvider.getApplicationContext(), product.price)))
            assertThat(fragment.tv_product_description.text.toString(),`is`(product.description))
            assertThat(fragment.img_product.imageAlpha,`is`(not(0)))
        }
    }

}