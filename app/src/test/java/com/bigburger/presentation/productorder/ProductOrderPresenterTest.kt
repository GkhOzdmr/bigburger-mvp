package com.bigburger.presentation.productorder

import com.bigburger.MockRepo
import com.bigburger.RxImmediateSchedulerRule
import com.bigburger.data.local.IngredientType
import com.bigburger.data.repository.ProductOrderRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertEquals
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsInstanceOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.internal.matchers.InstanceOf

class ProductOrderPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()
    @Mock
    lateinit var productOrderRepository: ProductOrderRepository
    @Mock
    lateinit var view: ProductOrderContract.View

    lateinit var productOrderPresenter: ProductOrderContract.Presenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        productOrderPresenter = ProductOrderPresenter(productOrderRepository)
        productOrderPresenter.takeView(view)
    }

    @After
    fun tearDown() {
        productOrderPresenter.onDestroy()
    }

    @Test
    fun setProductModel_CallsViewForDisplayingProduct() {
        // Given ProductModel
        val productModel = MockRepo.listOfProductModel[0]

        // When Presenter's ProductModel set
        `when`(productOrderRepository.getIngredients(com.nhaarman.mockitokotlin2.any()))
            .thenReturn(Observable.fromArray(MockRepo.listOfIngredientModel))

        productOrderPresenter.setProductModel(productModel)

        // Then verify View method called with ProductModel for displaying product
        verify(view).displayProduct(productModel)
    }

    @Test
    fun getIngredientsForProduct_ReturnsListOfIngredientModelAccordingToProductRef() {
        // When ingredient list wanted with given product ref
        `when`(productOrderRepository.getIngredients(com.nhaarman.mockitokotlin2.any()))
            .thenReturn(Observable.fromArray(MockRepo.listOfIngredientModel))
        productOrderPresenter.getIngredientsForProduct(1)

        // Then return list of ingredients that matches to ref,
        // display it on the view
        verify(productOrderRepository).getIngredients(IngredientType.HamburgerIngredients)
        verify(view).displayIngredients(com.nhaarman.mockitokotlin2.any())
    }

    @Test
    fun controlAndSetAmount_ControlsAmountFixesUnwantedConditionsAndSetsAmount() {
        // Setting up
        setProductModel()
        // When parameter is given the value = 0
        productOrderPresenter.controlAndSetAmount(0)

        // Then amount is set to 1
        val ACTUAL_AMOUNT_VALUE_FOR_0 = productOrderPresenter.getProductAmount()
        val EXPECTED_AMOUNT_VALUE_FOR_0 = 1
        assertEquals(EXPECTED_AMOUNT_VALUE_FOR_0, ACTUAL_AMOUNT_VALUE_FOR_0)

        // When parameter is given the value = 100
        productOrderPresenter.controlAndSetAmount(100)

        // Then amount is set to 1
        val ACTUAL_AMOUNT_VALUE_FOR_100 = productOrderPresenter.getProductAmount()
        val EXPECTED_AMOUNT_VALUE_FOR_100 = 99
        assertEquals(EXPECTED_AMOUNT_VALUE_FOR_100, ACTUAL_AMOUNT_VALUE_FOR_100)

        // Also view update for price change
        verify(view, atLeast(1)).updatePriceText(ArgumentMatchers.anyLong())
    }

    @Test
    fun decreaseProductAmount_RequestDecrementWhenValueIs1() {
        // Setting up
        setProductModel()
        controlAndSet(1)
        // hen amount is already 1 and decrement requested
        productOrderPresenter.decreaseProductAmount()

        // Then Sets amount to 1
        val ACTUAL_AMOUNT_VALUE = productOrderPresenter.getProductAmount()
        val EXPECTED_AMOUNT_VALUE = 1
        assertEquals(EXPECTED_AMOUNT_VALUE, ACTUAL_AMOUNT_VALUE)
    }

    @Test
    fun increaseProductAmount_RequestIncrementWhenAmountIs99() {
        // Setting up
        setProductModel()
        controlAndSet(99)
        // When amount is already 99 and increment requested
        productOrderPresenter.increaseProductAmount()

        // Then Sets amount to 99
        val ACTUAL_AMOUNT_VALUE = productOrderPresenter.getProductAmount()
        val EXPECTED_AMOUNT_VALUE = 99
        assertEquals(EXPECTED_AMOUNT_VALUE, ACTUAL_AMOUNT_VALUE)
    }

    @Test
    fun getIngredientString_CombinesListOfModelsIntoSingleStringAndReturns() {
        val ACTUAL_STRING_OBSERVABLE = productOrderPresenter.getIngredientString(MockRepo.listOfIngredientModel)
        val ingredientsString = "Tomato, Pickle, Onion, Ketchup, Mayonnaise"
        val EXPECTED_STRING_OBSERVABLE = Single.just(ingredientsString).subscribeOn(Schedulers.io())

        assertThat(ACTUAL_STRING_OBSERVABLE, IsInstanceOf(Single::class.java))
    }

    private fun setProductModel() {
        val productModel = MockRepo.listOfProductModel[0]
        `when`(productOrderRepository.getIngredients(com.nhaarman.mockitokotlin2.any()))
            .thenReturn(Observable.fromArray(MockRepo.listOfIngredientModel))
        productOrderPresenter.setProductModel(productModel)
    }

    private fun controlAndSet(amount: Int) {
        productOrderPresenter.controlAndSetAmount(amount)
    }
}