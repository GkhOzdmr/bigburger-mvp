package com.bigburger.presentation.product.fragment

import com.bigburger.MockRepo
import com.bigburger.TestUtils
import com.bigburger.presentation.MainActivity
import com.bigburger.presentation.product.ProductContract
import com.bigburger.presentation.productorder.fragment.ProductOrderFragment
import org.hamcrest.CoreMatchers
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ProductFragmentTest {

    @Mock
    private lateinit var presenterMock: ProductContract.Presenter

    private lateinit var mainActivity: MainActivity
    private lateinit var productFragment: ProductFragment

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        mainActivity = spy(MainActivity())
        productFragment = spy(ProductFragment.newInstance().apply { presenter=presenterMock })
    }

    @Test
    fun showLoading_CallsMainActivityForDisplayingLoading() {
        TestUtils.fragmentWithActivity(mainActivity,productFragment){ activity, fragment ->
            // When showLoading called at ProductFragment
            (fragment as ProductFragment).showLoading()

            // Then MainActivity's respected function is invoked
            verify(mainActivity).showLoading()
        }
    }

    @Test
    fun hideLoading_CallsMainActivityForDismissingLoading() {

    }

    @Test
    fun `adapter item click should navigate to product order fragment`() {
        TestUtils.fragmentWithActivity(mainActivity, productFragment) { activity, fragment ->
            // When Function invoked via Adapter Item click
            val itemFromAdapterClick = MockRepo.listOfProductModel[0]
            productFragment.productItemSelected(itemFromAdapterClick)

            // Then Navigates to ProductOrderFragment with respected ProductModel item
            val currentFragment = TestUtils.activityCurrentFragment(activity) as ProductOrderFragment
            val wantedFragment = CoreMatchers.isA(ProductOrderFragment::class.java)
            assertThat(currentFragment, wantedFragment)
        }
    }



}