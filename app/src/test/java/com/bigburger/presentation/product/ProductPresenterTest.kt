package com.bigburger.presentation.product

import com.bigburger.MockRepo
import com.bigburger.RxImmediateSchedulerRule
import com.bigburger.data.model.ProductModel
import com.bigburger.data.repository.ProductRepository
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import junit.framework.TestCase.assertEquals
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.*
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.mockito.verification.VerificationMode
import retrofit2.Response

class ProductPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    private lateinit var view: ProductContract.View

    @Spy
    private lateinit var productRepository: ProductRepository

    private lateinit var productPresenter: ProductContract.Presenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        productPresenter = ProductPresenter(productRepository)
        productPresenter.takeView(view)
    }

    @After
    fun tearDown() {
        productPresenter.dropView()
    }

    @Test
    fun getProducts_CallsViewMethods() {
        // Given a mock response of type List<ProductModel>
        val listOfProductModel = listOf<ProductModel>(
            ProductModel("1", "Title1", "Description1", "Thumbnail1", 0L),
            ProductModel("2", "Title2", "Description2", "Thumbnail2", 0L)
        )
        val testSubscribe = TestObserver<List<ProductModel>>()

        // When getProducts() called, return list of ProductModel from API
        `when`(productRepository.getProducts()).thenReturn(Single.just(Response.success(listOfProductModel)))
        productPresenter.getProducts()

        // Then make sure repository and view methods gets called
        verify(productRepository).getProducts()
        inOrder(view).apply {
            verify(view).showLoading()
            verify(view).hideLoading()
        }
    }

    @Test
    fun addToBasket_WithGivenModelNotExistsInTheList_AddsItemToList() {
        // When addToBasket called with given BasketProductModel that doesn't
        // already exist in the list
        productPresenter.addToBasket(MockRepo.listOfBasketProductModel[0])
        productPresenter.addToBasket(MockRepo.listOfBasketProductModel[1])

        // Then adds item to the list
        val ACTUAL_SIZE_OF_LIST = productPresenter.getBasketProducts().size
        val EXPECTED_SIZE_OF_LIST = 2
        assertEquals(EXPECTED_SIZE_OF_LIST, ACTUAL_SIZE_OF_LIST)
    }

    @Test
    fun addToBasket_WithGivenModelExistInTheList_UpdatesTheAmountValue() {
        // When addToBasket called with given BasketProductModel is
        // already exist in the list
        productPresenter.addToBasket(MockRepo.listOfBasketProductModel[0])
        productPresenter.addToBasket(MockRepo.listOfBasketProductModel[0])

        // Then updates the amount value
        val ACTUAL_AMOUNT_VALUE = productPresenter.getBasketProducts()[0].amount
        val EXPECTED_AMOUNT_VALUE = 4
        assertEquals(EXPECTED_AMOUNT_VALUE, ACTUAL_AMOUNT_VALUE)
    }


    @Test
    fun setBasketProduct_ReplacesBasketProductsListUpdatesBasketLogo() {
        // When list of BasketProductModel changed
        productPresenter.setBasketProducts(ArgumentMatchers.anyList())
        productPresenter.setBasketProducts(MockRepo.listOfBasketProductModel)

        // Then make sure last assignment is true
        assertEquals(MockRepo.listOfBasketProductModel, productPresenter.getBasketProducts())
        // And view called for updating logo
        verify(view, atLeast(1)).updateBasketLogo(ArgumentMatchers.anyInt())
    }

}