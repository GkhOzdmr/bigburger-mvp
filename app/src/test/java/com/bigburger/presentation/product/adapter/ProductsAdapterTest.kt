package com.bigburger.presentation.product.adapter

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import com.bigburger.MockRepo
import com.bigburger.R
import com.bigburger.TestUtils
import com.bigburger.presentation.MainActivity
import com.bigburger.presentation.product.ProductContract
import com.bigburger.presentation.product.fragment.ProductFragment
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ProductsAdapterTest {

    @Mock
    private lateinit var presenterMock: ProductContract.Presenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `adapter items should be correctly displayed`() {
        TestUtils.fragmentWithActivity(
            MainActivity(),
            ProductFragment.newInstance().apply { presenter = presenterMock }) {activity, fragment ->

            //
            val rcProducts = TestUtils.activityCurrentFragment(activity).view!!.findViewById<View>(R.id.rc_products) as RecyclerView
            val currentFragment = TestUtils.activityCurrentFragment(activity)

            //
            rcProducts.adapter = ProductsAdapter(fragment.context!!, currentFragment as ProductFragment)
            (rcProducts.adapter as ProductsAdapter).updateItems(MockRepo.listOfProductModel)

            //
            assertThat(rcProducts, `is`(notNullValue()))
            assertThat(rcProducts.adapter!!.itemCount, `is`(2))
        }
    }

/*
 @Test
    fun `adapter items should be correctly displayed`() {
        // Given the list of ProductModel & ProductAdapter instance with shadow context
        val listOfProductModel = MockRepo.listOfProductModel
        ActivityScenario.launch(MainActivity::class.java).onActivity {
            it.changeFragment(ProductFragment.newInstance().apply { presenter = presenterMock }, false)
            //
            val rcProducts = TestUtils.activityCurrentFragment(it).view!!.findViewById<View>(R.id.rc_products) as RecyclerView
            val currentFragment = TestUtils.activityCurrentFragment(it)

            //
            rcProducts.adapter = ProductsAdapter(it.baseContext, currentFragment as ProductFragment)
            (rcProducts.adapter as ProductsAdapter).updateItems(MockRepo.listOfProductModel)

            //
            assertThat(rcProducts, `is`(notNullValue()))
            assertThat(rcProducts.adapter!!.itemCount, `is`(2))
        }
    }*/

}