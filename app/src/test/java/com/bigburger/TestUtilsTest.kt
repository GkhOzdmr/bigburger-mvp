package com.bigburger

import com.bigburger.data.model.IngredientModel
import com.bigburger.presentation.MainActivity
import com.bigburger.presentation.product.ProductContract
import com.bigburger.presentation.product.fragment.ProductFragment
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import org.apache.tools.ant.Main
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.robolectric.RobolectricTestRunner
import java.util.*

@RunWith(RobolectricTestRunner::class)
class TestUtilsTest {

    @Mock
    private lateinit var presenterMock: ProductContract.Presenter

    @Spy
    private val mainActivity = MainActivity()

    @Spy
    private lateinit var productFragment: ProductFragment

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        productFragment = ProductFragment.newInstance().apply { presenter = presenterMock }
    }

    @Test
    fun `activity and fragment should not be null`() {
        TestUtils.fragmentWithActivity(
            mainActivity,
            productFragment
            ) { activity, fragment ->

            assertThat(activity as MainActivity, `is`(notNullValue()))
            assertThat(fragment as ProductFragment, `is`(notNullValue()))

        }
    }

}