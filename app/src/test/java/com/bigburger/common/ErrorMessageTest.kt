package com.bigburger.common

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.bigburger.R
import com.bigburger.data.remote.NetworkErrorType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner


/**
 * Test suite for [ErrorMessage]
 */
@RunWith(RobolectricTestRunner::class)
class ErrorMessageTest {

    @Test
    fun getMessage_GivenErrorType_ReturnsErrorModel() {
        // When getMessage called with ErrorType
        val errorMessage =
            ErrorMessage.getMessage(ApplicationProvider.getApplicationContext(), NetworkErrorType.UnknownError)

        // Then matching error header & message should return
        val expectedErrorMessage = (ApplicationProvider.getApplicationContext() as Context).getString(R.string.error_unknown_message)
        val expectedErrorHeader = (ApplicationProvider.getApplicationContext() as Context).getString(R.string.error_unknown_header)
        assertThat(errorMessage.errorHeader, `is`(equalTo(expectedErrorHeader)))
        assertThat(errorMessage.errorMessage, `is`(equalTo(expectedErrorMessage)))
    }
}