package com.bigburger.common

import androidx.test.core.app.ApplicationProvider
import com.bigburger.base.BigBurgerApp
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
class PriceUtilTest {

    @Test
    fun `rawPrice_PriceAsLongAtParameter_ReturnsPriceStringDecimalized`() {
        // Given Price in Long
        val priceAsLong2Digit = 10L
        val priceAsLong4Digit = 1000L
        val priceAsLong6Digit = 100000L

        // When rawPriceToString called with given prices
        val resultof2Digit = PriceUtil.rawPriceToString(priceAsLong2Digit)
        val resultOf4Digit = PriceUtil.rawPriceToString(priceAsLong4Digit)
        val resultOf6Digit = PriceUtil.rawPriceToString(priceAsLong6Digit)

        // Then make sure price will be correctly decimalized
        assertThat(resultof2Digit,`is`(equalTo("10")))
        assertThat(resultOf4Digit,`is`(equalTo("10.00")))
        assertThat(resultOf6Digit,`is`(equalTo("1000.00")))
    }

    @Test
    fun priceWithSymbol_PriceAsLongAtParameter_DecimalizedStringWithSymbol() {
        // Given Price in Long
        val priceAsLong2Digit = 10L
        val priceAsLong4Digit = 1000L
        val priceAsLong6Digit = 100000L

        // When priceWithSymbol called with given prices
        val appContext = ApplicationProvider.getApplicationContext<BigBurgerApp>()
        val resultof2Digit = PriceUtil.priceWithSymbol(appContext, priceAsLong2Digit)
        val resultOf4Digit = PriceUtil.priceWithSymbol(appContext, priceAsLong4Digit)
        val resultOf6Digit = PriceUtil.priceWithSymbol(appContext, priceAsLong6Digit)

        // Then Price will be correctly decimalized and symbolized
        assertThat(resultof2Digit,`is`(equalTo("10 Krş")))
        assertThat(resultOf4Digit,`is`(equalTo("10.00 TL")))
        assertThat(resultOf6Digit,`is`(equalTo("1000.00 TL")))
    }
}