package com.bigburger

import android.R
import android.text.Layout
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.test.core.app.ActivityScenario
import com.bigburger.presentation.MainActivity
import com.bigburger.presentation.product.fragment.ProductFragment
import kotlin.reflect.KClass

/**
 * Kotlin Object for common Testing utilities
 */
object TestUtils {

    /**
     * Returns the latest fragment in the Back Stack of the given Activity
     *
     * @param activity activity to request latest fragment
     *
     * @return latest fragment at the back stack of the activity
     */
    fun activityCurrentFragment(activity: AppCompatActivity): Fragment {
        val fragmentManager = activity.supportFragmentManager
        val lastBackStackEntry = fragmentManager.backStackEntryCount - 1
        val currentFragmentTag = fragmentManager.getBackStackEntryAt(lastBackStackEntry).name

        return fragmentManager.findFragmentByTag(currentFragmentTag)!!
    }

    /**
     * Opens a Fragment from specified Activity as container, allows you to code within opened fragment
     *
     * @param activity Activity as container for Fragment
     * @param fragment Fragment to be displayed
     * @param onFragment lambda inline method for doing business on opened fragment
     */
    inline fun fragmentWithActivity(activity: AppCompatActivity,fragment: Fragment,crossinline onFragment: (activity: AppCompatActivity, fragment: Fragment) -> Unit) {
        ActivityScenario.launch(activity.javaClass).onActivity {

            val dummyContainer = FrameLayout(it.baseContext).apply{
                id=1
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            }
            (it.findViewById<View>(R.id.content) as ViewGroup).addView(dummyContainer)

            val fragmentManager = it.supportFragmentManager.beginTransaction()

            fragmentManager.replace(dummyContainer.id,fragment,fragment.javaClass.simpleName)
            fragmentManager.addToBackStack(fragment.javaClass.simpleName)
            fragmentManager.commit()


            onFragment(it, fragment)
        }
    }

}