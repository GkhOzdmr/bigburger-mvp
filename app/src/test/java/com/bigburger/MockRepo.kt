package com.bigburger

import com.bigburger.data.model.BasketProductModel
import com.bigburger.data.model.IngredientModel
import com.bigburger.data.model.ProductModel

class MockRepo {
    companion object {

        val listOfProductModel = listOf<ProductModel>(
            ProductModel("1", "Title1", "Description1", "Thumbnail1", 0L),
            ProductModel("2", "Title2", "Description2", "Thumbnail2", 0L)
        )

        val listOfIngredientModel = listOf<IngredientModel>(
            IngredientModel("Tomato"),
            IngredientModel("Pickle"),
            IngredientModel("Onion"),
            IngredientModel("Ketchup"),
            IngredientModel("Mayonnaise")
        )

        val listOfBasketProductModel = listOf<BasketProductModel>(
            BasketProductModel(listOfProductModel[0], listOfIngredientModel[0].ingredientName,2),
            BasketProductModel(listOfProductModel[1], listOfIngredientModel[3].ingredientName,5),
            BasketProductModel(listOfProductModel[1], listOfIngredientModel[2].ingredientName,3)
        )

        val arrayListOfBasketProductModel = arrayListOf<BasketProductModel>(
            BasketProductModel(listOfProductModel[0], listOfIngredientModel[0].ingredientName,2),
            BasketProductModel(listOfProductModel[1], listOfIngredientModel[3].ingredientName,5),
            BasketProductModel(listOfProductModel[1], listOfIngredientModel[2].ingredientName,3)
        )

    }
}