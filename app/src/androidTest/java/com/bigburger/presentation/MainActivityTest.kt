package com.bigburger.presentation

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bigburger.R
import org.hamcrest.CoreMatchers.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Test
    fun showLoading_ShouldDisplay_loading_layout() {
        // Given the MainActivity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        // When showLoading called on MainActivity
        activityScenario.onActivity {
            it.showLoading()
        }


        // Then make sure loading_layout is visible
        onView(withId(R.id.loading)).check(matches(isDisplayed()))
    }

    @Test
    fun hideLoading_ShouldDismiss_loading_layout() {
        // Given the MainActivity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        // When hideLoading called on MainActivity after the showLoading called
        activityScenario.onActivity {
            it.showLoading()
            it.hideLoading()
        }

        // Then make sure loading_layout is NOT visible
        onView(withId(R.id.loading)).check(matches(not(isDisplayed())))
    }

}